using System;
using System.Collections.Generic;
using System.Diagnostics;
using static System.Double;
using static System.Math;
using static System.Console;
public class eig{
public static int rank1(matrix D, vector u, vector e, double c=1,
		int n0=0, int n=-1, double acc=1e-3){
	int nop = 0, N=u.size;
	Trace.Assert(D.size1==D.size2,"The diagonal matrix must be square");
	Trace.Assert(D.size1==u.size,"The matrix and the vector must have the same dimension");
	// Sorting the elements of the diagonal matrix D
	vector d = new vector(N);
	for(int i=0;i<N;i++) d[i] = D[i,i];
	sort.quicksort(d,0,n-1);
	// If c is 0 A=D such that the eigen values are just the elements of d
	if(c==0){
		Error.Write("c=0");
		for(int i=n0;i<n0+n;i++) e[i] = d[i];
		return nop;
	}//if
	// n is the number of desired eigen values where the default n=-1 is used for
	// all eigen values as it cannot have default value n=u.size. That is set here.
	if(n<0) n=u.size;
	// List of indices of the elements of u, whose absolute value is greater than the desiered accuracy
	List<int> m = new List<int>();
	for(int i=0;i<N;i++) if(!cmath.approx(u[i],0,acc:acc)) m.Add(i);
	Write("\nLists used to generate initial value for Newton's method:\n");
	d.print("d: ");
	// Secular equation
	Func<double,double> sec_eq = delegate(double x){
		nop++;
		double sum = 1;
		foreach(int i in m) sum *= D[i,i]-x;
		foreach(int i in m){
			double aux = c*u[i]*u[i];
			foreach(int j in m){
				if(i!=j) aux *= D[j,j]-x;
			}//foreach
			sum += aux;
		}//foreach
		return sum;
	};//sec_eq
	// Setting up vectors to print the information for the starting valuees for Newton's method
	vector x0 = new vector(n);
	vector y0 = new vector(n);
	// Calculating n eigen values starting from n0
	for(int i=n0;i<n0+n;i++){
		// Calculating interval in which the eigen value must be
		double a,b;
		if(c>0){
			a = d[i];
			if(i==n-1) b = a+c*u.norm()/2;
			else b = d[i+1];
		}//if
		else{
			b = d[i];
			if(i==0) a = b+c*u.norm()/2;
			else a = d[i-1];
		}//if
		// Calculating yi
		double y = (a+b)/2;
		// Setting the initial point xi to di
		double x = d[i];
		// If |f(yi)|<|f(di)| xi is set to yi
		if(Abs(sec_eq(y))<Abs(sec_eq(x))) x = y;
		// Adding yi and xi to their respective lists
		y0[i] = y;
		x0[i] = x;
		// Finding the root of the secular equation
		double res = roots.newton(sec_eq,x,acc:acc,dx:acc*1e-3);
		if(res<a || res>b){
			Error.Write("Root outside the correct interval found. Trying new starting point.\n");
			if(Abs(sec_eq(y))>=Abs(sec_eq(x))) x = y;
			else x=d[i];
			res = roots.newton(sec_eq,x,acc:acc,dx:acc*1e-3);
		}//if
		e[i] = res;
	}//for
	y0.print("y: ");
	x0.print("x: ");
	return nop;
}//rank1
}//eig
