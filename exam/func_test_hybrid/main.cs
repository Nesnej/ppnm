using System;
using System.Collections.Generic;
using static System.Math;
using static System.Console;
public class main{
public static void Main(){
	int n = 40;
	// The random number generator is seeded, such that the same plot is produced
	// each time make is run. This is done to make sure that the comments on the plot
	// in ms.pdf are appropriate
	var rnd = new Random(1);
	matrix A = new matrix(n,n);
	vector e = new vector(n);
	vector u = new vector(n);
	matrix V = new matrix(n,n);
	for(int i=0;i<n;i++){
		A[i,i] = 10*(rnd.NextDouble() - .5);
		u[i] = 2*(rnd.NextDouble() - .5);
	}//for
	double c = 2*(rnd.NextDouble() -.5);
	matrix B = A + c*matrix.outer(u,u);
	jacobi.cyclic(B,e,V);
	List<int> m = new List<int>();
	for(int i=0;i<n;i++) if(u[i]!=0) m.Add(i);
	Func<double,double> sec_eq = delegate(double x){
		double sum = 1;
		foreach(int i in m) sum *= A[i,i]-x;
		foreach(int i in m){
			double aux = c*u[i]*u[i];
			foreach(int j in m){
				if(i!=j) aux *= A[j,j]-x;
			}//foreach
			sum += aux;
		}//foreach
		return sum;
	};//sec_eq
	Func<double,double> sec_eq_log = delegate(double x){
		double sum = 1;
		foreach(int i in m) sum *= A[i,i]-x;
		foreach(int i in m){
			double aux = c*u[i]*u[i];
			foreach(int j in m){
				if(i!=j) aux *= A[j,j]-x;
			}//foreach
			sum += aux;
		}//foreach
		return Sign(sum)*Log10(Abs(sum));
	};//sec_eq
	for(double x=e[0]-.5;x<e[n-1]+.5;x+=1e-3)
		Write($"{x} {sec_eq_log(x)}\n");
	Write("\n\n");
	for(int i=0;i<n;i++) Write($"{e[i]} {0}\n");
	Write("\n\n");
	for(int i=0;i<n;i++)
		Write($"{A[i,i]} {sec_eq_log(A[i,i])}\n");
	Write("\n\n");
	vector d = new vector(n);
	for(int i=0;i<n;i++) d[i] = A[i,i];
	sort.quicksort(d,0,n-1);
	int nmax = Convert.ToInt32(n/4);
	if(nmax<3) nmax=3;
	for(int i=0;i<n;i++){
		double a,b;
		if(c>0){
			a = d[i];
			if(i==n-1) b = a+c*u.norm();
			else b = d[i+1];
		}//if
		else{
			b = d[i];
			if(i==0) a = b+c*u.norm();
			else a = d[i-1];
		}//if
		double x0 = roots.bisection_newton(sec_eq,a,b,nmax:nmax);
		Write($"{x0} {sec_eq_log(x0)}\n");
	}//for
	double edge=0;
	if(c>0) edge=d[n-1]+c*u.norm();
	else edge=d[0]+c*u.norm();
	Write($"\n\n{edge} {sec_eq_log(edge)}\n");
}//Main
}//main
