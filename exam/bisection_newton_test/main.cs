using System;
using static System.Console;
using static System.Math;
public static class main{
public static void Main(){
	Write("Test of the Combined Bisection and Newton Methods\n\n");
	
	var rnd = new Random(1);
	int N=0, nmax=3;
	double acc = 1e-12;
	double a = PI*(rnd.NextDouble() - 1);
	double b = PI*rnd.NextDouble();
	Func<double,double> sin=(x) => Sin(x);
	double root = rootss.bisection_newton(sin,a,b,acc,log:true,nmax:nmax);
	double exact = 0;
	Write("Sine Test:\n");
	N += tester(sin,a,b,root,exact,acc);
	if(Abs(exact-root)>acc && Abs(root-exact)%PI<acc){
		Write("OBS: A different root than the nearest has been found!\n");
		Write($"Absolute Error mod pi : {Abs(root-exact)%PI}\n");
	}//if

	Write("\n");

	Func<double,double> cos=(x) => Cos(x);
	a = PI*(rnd.NextDouble() - 3D/2);
	b = PI*(rnd.NextDouble() - 1D/2);
	exact = -PI/2;
	root = rootss.bisection_newton(cos,a,b,acc,log:true,nmax:nmax);
	Write("Cosine Test:\n");
	N += tester(cos,a,b,root,exact,acc);
	if(Abs(exact-root)>acc && Abs(root-exact)%PI<acc){
		Write("OBS: A different root than the nearest has been found!\n");
		Write($"Absolute Error mod pi : {Abs(root-exact)%PI}\n");
	}//if

	Write("\n");

	Func<double,double> pol=(x) => x*x*x;
	a = 5*(rnd.NextDouble() - 1);
	b = 5*rnd.NextDouble();
	exact = 0;
	root = rootss.bisection_newton(pol,a,b,acc,log:true,nmax:nmax);
	Write("Third Order Polynomium Test:\n");
	N += tester(pol,a,b,root,exact,acc);

	if(N==0) Write("ALL TESTS PASSED!\n");
	else Write("NOT ALL TESTS PASSED!\n");
}//Main

public static int tester(Func<double,double> f, double a, double b, double root, double exact, double acc){
	var err = exact - root;
	var dif = f(exact) - f(root);
	Write($"Starting Interval: [{a}, {b}]\n");
	Write($"Numerical Root   : {root}\n");
	Write($"Analytical Root  : {exact}\n");
	Write($"Error     	 : {Abs(err)}\n");
	Write($"||f(root)||      : {Abs(dif)}\n");
	Write($"Tolerance        : {acc}\n");
	if(dif<acc) {Write("TEST PASSED!\n"); return 0;}
	else Write("TEST FAILED!\n");
	return 1;
}//tester
}//main
