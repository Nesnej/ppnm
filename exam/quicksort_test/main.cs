using System;
using static System.Console;
public class main{
public static void Main(){
	int n = 5;
	var rnd = new Random(1);
	vector v = new vector(n);
	for(int i=0;i<n;i++) v[i] = 2*(rnd.NextDouble()-.5);
	Write("Test of sorting algorithm\n");
	v.print("Initial vector ");
	sort.quicksort(v,0,n-1);
	v.print("Sorted vector  ");
	int crit=n-1;
	for(int i=1;i<n;i++) if(v[i-1]<=v[i]) crit--;
	if(crit==0) Write("TEST PASSED!\n");
	else Write("TEST FAILED!\n");
}//Main
}//main
