using System;
using System.Diagnostics;
using static System.Math;
using static System.Double;
using static System.Console;
public class main{
public static void Main(){
	int n0=3, nmax = 21;
	// It apparently takes longer to run a method in a class the first times,
	// thus a dummy run on an identity matrix is done to avoid this problem.
	matrix Q = new matrix(n0,n0); Q.setid();
	vector eigval = new vector(n0);
	matrix vec = new matrix(n0,n0);
	jacobi.cyclic(Q,eigval,vec);
	jacobi.classic(Q,eigval,vec);
	eig.rank1_newton(Q,eigval,eigval);
	eig.rank1_bisection(Q,eigval,eigval);
	eig.rank1_hybrid(Q,eigval,eigval);
	var watch=new Stopwatch();
	watch.Reset();
	watch.Start();
	watch.Stop();
	double acc = 1e-3;
	for(int n = n0;n<=nmax;n++){
		var rnd = new Random(1);
		matrix D = new matrix(n,n);
		vector e = new vector(n);
		vector u = new vector(n);
		matrix V = new matrix(n,n);
		for(int i=0;i<n;i++){
			D[i,i] = 10*(rnd.NextDouble() - .5);
			u[i] = 2*(rnd.NextDouble() - .5);
		}//for
		double c = 2*(rnd.NextDouble() -.5);
		matrix A = D + c*matrix.outer(u,u);
		matrix B = A.copy();
		watch.Reset();
		watch.Start();
		int N = jacobi.cyclic(A,e,V);
		watch.Stop();
		Write($"{n} {N*n} {watch.ElapsedTicks} ");
		watch.Reset();
		watch.Start();
		N = jacobi.classic(B,e,V);
		watch.Stop();
		Write($"{N} {watch.ElapsedTicks} ");
		if(n<20){
			watch.Reset();
			watch.Start();
			N = eig.rank1_newton(D,u,e,c:c,n0:0,n:n,acc:acc);
			watch.Stop();
			Write($"{N} {watch.ElapsedTicks} ");
		}//if
		else Write($"{NaN} {NaN} ");
		if(n<17){
			watch.Reset();
			watch.Start();
			N = eig.rank1_bisection(D,u,e,c:c,n0:0,n:n,acc:acc);
			watch.Stop();
			Write($"{N} {watch.ElapsedTicks} ");
		}//if
		else Write($"{NaN} {NaN} ");
		if(n<18){
			watch.Reset();
			watch.Start();
			N = eig.rank1_hybrid(D,u,e,c:c,n0:0,n:n,acc:acc);
			watch.Stop();
			Write($"{N} {watch.ElapsedTicks}\n");
		}//if
		else Write($"{NaN} {NaN}\n");
	}//for
}//Main
}//main
