using System;
using static System.Math;
using static System.Console;
public class roots{
public static double newton(Func<double,double> f, double x,
		double acc=1e-3, double dx=1e-7){
	double lambda_min = 1D/64;
	// Setting up derivative and point for function evaluation
	double fx = f(x);
	double z,fz,J;
	// Do Newton step until convergence
	while(Abs(fx)>acc){
		// Calculating derivative
		J = (f(x+dx)-fx)/dx;
		// Calculating Newton full step
		double lambda = 1D;
		z = x-lambda*fx/J;
		fz = f(z);
		// Doing modified Newton step
		while(Abs(fz)>(1-lambda/2)*Abs(fx)){
			if(lambda>lambda_min){
				lambda /= 2;
				fz = f(x-lambda*fx/J);
			}//if
			// Breaking if too many step attempts are bad
			else {Error.Write($"bad point: lambda={lambda_min}\n"); break;}
		}//while
		// Updating root estimate
		x -= lambda*fx/J;
		fx = f(x);
	}//while
	return x;
}//newton
}//root
