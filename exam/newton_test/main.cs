using System;
using static System.Console;
using static System.Math;
public static class main{
public static void Main(){
	Write("Test of Newton's method for functions of type Func<double,double>\n\n");
	
	var rnd = new Random(1);
	int N=0;
	double acc = 1e-12;
	Func<double,double> ho1d=(x) => x*x;
	double x01d = 5*(rnd.NextDouble() - .5);
	double root1d = roots.newton(ho1d,x01d,acc);
	double exact1d = 0;
	Write("1D Harmonic Oscillator Test:\n");
	N += tester(ho1d,x01d,root1d,exact1d,acc);
	
	Write("\n");

	Func<double,double> sin=(x) => Sin(x);
	double x0s = 1;
	double root_s = roots.newton(sin,x0s,acc);
	double exacts = 0;
	Write("1D Sine Test:\n");
	N += tester(sin,x0s,root_s,exacts,acc);
	if(Abs(exacts-root_s)>acc && Abs(root_s-exacts)%PI<acc){
		Write("OBS: A different root than the nearest has been found!\n");
		Write($"Absolute Error mod pi : {Abs(root_s-exacts)%PI}\n");
	}//if

	Write("\n");

	Func<double,double> cos=(x) => Cos(x);
	double exactc = -PI/2;
	double x0c = -.1;
	for(int i=0;i<2;i++){
		double rootc = roots.newton(cos,x0c,acc);
		Write("1D Cosine Test:\n");
		N += tester(cos,x0c,rootc,exactc,acc);
		if(Abs(exactc-rootc)>acc && Abs(rootc-exactc)%PI<acc){
			Write("OBS: A different root than the nearest has been found!\n");
			Write($"Absolute Error mod pi : {Abs(rootc-exactc)%PI}\n");
		}//if
		x0c *= 10;
		Write("\n");
	}//for

	if(N==0) Write("ALL TESTS PASSED!\n");
	else Write("NOT ALL TESTS PASSED!\n");
}//Main

public static int tester(Func<double,double> f, double x0, double root, double exact, double acc){
	var err = exact - root;
	var dif = f(exact) - f(root);
	Write($"Starting Point : {x0}\n");
	Write($"Numerical Root : {root}\n");
	Write($"Analytical Root: {exact}\n");
	Write($"Error          : {Abs(err)}\n");
	Write($"||f(root)||    : {Abs(dif)}\n");
	Write($"Tolerance      : {acc}\n");
	if(dif<acc) {Write("TEST PASSED!\n"); return 0;}
	else Write("TEST FAILED!\n");
	return 1;
}//tester
}//main
