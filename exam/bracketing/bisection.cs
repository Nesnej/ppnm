using System;
using System.Diagnostics;
using static System.Math;
using static System.Double;
public class roots{
public static double[] bisection(Func<double,double> f, double a, double b,
		double acc=1e-3, double fa=NaN, double fb=NaN){
	// Setting the function values at the interval end points if they are not provided
	if(IsNaN(fa)) fa = f(a);
	if(IsNaN(fb)) fb = f(b);
	Trace.Assert(a!=b,"The starting interval must contain more than one point");
	Trace.Assert(a<b,"The starting interval must be ordered with the first point smaller than the last");
	Trace.Assert(Sign(fa)!=Sign(fb),"The sign of the function evaluated at the endpoints must be different");
	// Midpoint and function value at the midpoint
	double c=(b+a)/2, fc=f(c);
	// Half interval size
	double dx=(b-a)/2;
	// If the interval size is smaller than tolerance there is an interval of size
	// dx smaller than tolerance, in which the root is
	if(dx<=acc){
		// If a and c have opposite signs the root is in [a,c]
		if(Sign(fa)!=Sign(fc)){
			// Is f(a) is smaller than tolerance?
			if(Abs(fa)<acc){
				// Is c a better estimate than a?
				if(Abs(fc)<Abs(fa)) return new double[] {c,dx};
				// If not return a
				else return new double[] {a,dx};
			}//if
		}//if
		// If a and c have the same sign the root is in [c,b]
		else{
			// Is f(b) is smaller than tolerance?
			if(Abs(fb)<acc){
				// Is c a better estimate than b?
				if(Abs(fc)<Abs(fb)) return new double[] {c,dx};
				// If not return b
				else return new double[] {b,dx};
			}//if
		}//else
	}//if
	// If not converged continue recursively on the interval with function values
	// of opposite signs and the edges
	if(Sign(fa)==Sign(fc)) return bisection(f,c,b,acc:acc,fa:fc,fb:fb);
	else return bisection(f,a,c,acc:acc,fa:fa,fb:fc);
}//bisection
}//root
