\documentclass[a4paper,english,twocolumn]{article}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{graphicx}
\usepackage{verbatim}
\title{Diagonalization of Rank-1 Updated Diagonal Matrix\\
\Large Practical Programming and Numerical Methods\\
Examination Project}
\author{Rasmus~Berg~Jensen\\201608612}
\date{\today}
\begin{document}
\maketitle
\noindent
\section{Introduction}
A size $n$ real matrix, $A$, on the form
%
\begin{equation} \label{eq:Aform}
	A = D + cuu^\intercal,
\end{equation}
%
where $D$ is a diagonal size $n$ matrix, $u$ is size $n$ vector and $c$ is a real number, can be diagonalized numerically in $\mathcal O(n^2)$ operations. This can be done by solving the secular equation in $\lambda$ \cite{fedorovAnotherIntroductionNumerical}
%
\begin{equation} \label{eq:sec_eq_1}
	1 + \sum_{i=1}^m \frac{cu_i^2}{D_{ii}-\lambda} = 0,
\end{equation}
%
where $D_{ii}$ is the $i$th element of $D$ and $m$ is the number of non-zero elements of $u$. Equation \eqref{eq:sec_eq_1} can equivalently be expressed as
%
\begin{equation} \label{eq:sec_eq_2}
	f(\lambda) = \prod_{i=1}^m(D_{ii}-\lambda) + \sum_{i=1}^m \prod_{j\neq i} cu_i^2(D_{jj}-\lambda) = 0,
\end{equation}
%
such that one avoids the potential singularity of $1/(d_i-\lambda)$. For convenience the lefthand side of equation \eqref{eq:sec_eq_2} is named $f(\lambda)$. The eigen values of $D$, $\{d_i\}$, are found by sorting the elements $\{D_{ii}\}$ after which the roots of equation \eqref{eq:sec_eq_2} can be found using Newton's method, see \cite{fedorovAnotherIntroductionNumerical}.

\section{Initial Value} \label{sec:init_val}
Newton's method needs an initial value from which it finds, in most cases, the nearest root assuming convergence is reached. A reliable way of generating a good initial guess for the $i$th eigen value of $A$, which will be denoted $\lambda_i$, is thus needed for the algorithm to work properly. If the elements of the matrix $cuu^\intercal$ is small compared to the elements of $D$, $\lambda_i$ will be close to $d_i$, hence $d_i$ is a good starting guess. That is however not the case generally, so a better approach is necessary. The eigen values of $A$ are bounded \cite{fedorovAnotherIntroductionNumerical} to the following intervals. \\
If $c>0$ then for $i=1,...,n-1$
%
\begin{equation} \label{eq:int_a}
	d_i \leq \lambda_i \leq d_{i+1}
\end{equation}
%
and
%
\begin{equation}
	d_n \leq \lambda_n \leq d_{n} + cu^\intercal u.
\end{equation}
%
If $c<0$ then for $i=2,...,n$
%
\begin{equation}
	d_{i-1} \leq \lambda_i \leq d_{i}
\end{equation}
%
and
%
\begin{equation} \label{eq:int_b}
	d_1 + cu^\intercal u \leq \lambda_1 \leq d_{1}.
\end{equation}
%
Newtons method is not designed to find a root inside an interval, so equations (\ref{eq:int_a}-\ref{eq:int_b}) cannot be used directly to find the desired eigen value in the general case. There exist algorithms to find a root inside an interval, but these have not been covered in the course so we will stick to Newton's method for the first implementation. The observation on which the bisection method \cite{RootfindingAlgorithms2020} is based will be exploited, namely that for a continous real function $f(x)$ if an interval $[a,b]$ fulfills that	
%
\begin{equation} \label{eq:root_cond}
	\mathrm{sign}[f(a)] \neq \mathrm{sign}[f(b)],
\end{equation}
%
then there exist at least one root inside the interval. For the intervals in equations (\ref{eq:int_a}-\ref{eq:int_b}) equation \eqref{eq:root_cond} is fulfilled for the function on the lefthand side of equation \eqref{eq:sec_eq_2}, $f(\lambda)$, as it is a polynomium with exactly one root in each interval. Let $y_i$ denote the midpoint of the interval for $\lambda_i$ in equations (\ref{eq:int_a}-\ref{eq:int_b}). In most cases Newton's method will converge with either $d_i$ or $y_i$ as the starting point, thus the starting point is chosen as
%
\begin{equation} \label{eq:init}
	x_i =
	\begin{cases}
		d_i, \enspace &|f(d_i)| < |f(y_i)| \\
		y_i, \enspace &|f(y_i)| \leq |f(d_i)|
	\end{cases}
	.
\end{equation}
%
Figure \ref{fig:func_test} displays the function $f(\lambda)$ for the secular equation of equation \eqref{eq:sec_eq_2}, but the point $d_0+cu^\intercal u = -5.2$ and thus far to the left of the region of interest. It is noted that for this particular matrix the eigen values found using the cyclic Jacobi algorithm match the roots of the secular equation and that they are either close to $d_i$ or $y_i$ indicating that $x_i$ is a reasonable starting point. Through trial and error it was found that choosing the starting point through equation \eqref{eq:init} does indeed work in most cases but not always. After finding a root of the secular equation, one can check if it is inside the the correct interval in equations (\ref{eq:int_a}-\ref{eq:int_b}). If that is the case, the root is accepted as the correct eigen value, otherwise the rejected option from equation \eqref{eq:init} is used. In practice this turns out to work quite well.\\ 

\begin{figure}[t]
	\resizebox{\columnwidth}{!}{\input{Test.tex}}
	\caption{The function $f(\lambda)$ for a matrix A on the form of equation \eqref{eq:Aform} with randomly generated matrix elements.}
	\label{fig:func_test}
\end{figure}

\noindent
Newton's method uses the convergence criteria $|f(x)|<\tau$ where $\tau$ is some preset tolerance. For this implementation $\tau$ is passed to the method as an input parameter. Additionally the implementation of Newton's method used here allows for the user to set the finite difference, $dx$, used to estimate the Jacobian, which determines the direction of the Newton step and the trial step size for the line search. This is set to $0.1\%$ of tolerance,
%
\begin{equation}
	dx = \frac{\tau}{1\cdot10^3},
\end{equation}
%
as a conservative estimate such that the accuracy of the gradient matches the desired accuracy of the eigen values.

\begin{figure*}
	\verbatiminput{quicksort_test/out.txt}
	\caption{Output from the test of the quicksort algorithm.}
	\label{fig:quicksort}
\end{figure*}
%
\section{Sorting}
The elements $\{D_{ii}\}$ must be sorted to obtain $\{d_i\}$ as required by equation \eqref{eq:init}. In order to obtain the desired $\mathcal O(n^2)$ scaling the sorting algorithm used to achieve this must be $\mathcal O(n^2)$ or better and here Lomutos partition scheme for the quicksort algorithm \cite{Quicksort2020} has been used. The implementation of this algorithm can be see in the file \verb|sort.cs| in the \verb|quicksort_test| directory, where it is also tested resulting in the output of figure \ref{fig:quicksort}.

\begin{figure*}
	\verbatiminput{single_matrix_test/out.txt}
	\caption{Output from the diagonalization test of the symmetric rank-1 updated matrix method using Newton's method for root-finding.}
	\label{fig:test_res}
\end{figure*}
%
\section{Test of the Implimentation} \label{sec:test}
In the directory \verb|single_matrix_test| the diagonalization algorithm hereto descriped is implimented in the file \verb|eig_rank1.cs|, where a test of the method is also run. The test sets up a matrix on the form of equation \eqref{eq:Aform} using random numbers from the C\# random number generator. The resulting $A$-matrix is then diagonalized both using this method and the cyclic Jacobi method and the results are seen in figure \ref{fig:test_res}. A plot like the one in figure \ref{fig:func_test} is also produced during the test, which is a visual representation of the root finding part of the method, and can be seen in the test directory, as it contains very limited information not seen in figure \ref{fig:test_res}. As $\tau$is the tolerance of the size of $f(\lambda)$ rather than on the error of the root there is a risk that the figure will print ``TEST FAILED'' as found root is further from the Jacobi eigen value than the desired tolerance. In that case check the printed differences in eigen values which should not be larger than tolerance by more than a small factor.

\begin{figure}[t]
	\resizebox{\columnwidth}{!}{\input{Scale_n_n.tex}}
	\caption{Scaling of the number of operations for the symmetric rank-1 method.}
	\label{fig:scaling_n}
\end{figure}
%
\begin{figure}[t]
	\resizebox{\columnwidth}{!}{\input{Scale_n_t.tex}}
	\caption{Scaling of time of computation for the symmetric rank-1 method.}
	\label{fig:scaling_t}
\end{figure}
%
\subsection{Scaling of the Method}
A test of the scaling of the method with the matrix dimension was performed in the directory \verb|scale_n|, which results in figures \ref{fig:scaling_n} and \ref{fig:scaling_t}. %The Jacobi algorithms uses $\mathcal O(n^3)$ operations to achieve convergence. Therefore a 2nd and 3rd order polynomium that goes through the first point of the cyclic Jacobi and 
A parabola that goes through the first datapoint is plotted in figure \ref{fig:scaling_n} and in figure \ref{fig:scaling_t} the parabola is fitted to the last point. It is seen that in figure \ref{fig:scaling_n} the parabola grows more rapidly than the number of operations and hence the number of operations scales better than $\mathcal O(n^2)$. Figure \ref{fig:scaling_t} tells the same story, where the elapsed time grows slower than the desired scaling. In practice it turned out to be safer to set the parabola to the last datapoint as the time of the first datapoint is more sensitive than the last.

\begin{figure*}
	\verbatiminput{bracketing/print.txt}
	\caption{Output from the first test of the bisection algorithm.}
	\label{fig:bisection}
\end{figure*}
%
\section{Improvement of the Method}
The most obvious way improve the method is to use a more suitable root finding method. For simplicity, and as a proof of concept, the bisection method \cite{RootfindingAlgorithms2020} is implemented. This method exploids equation \eqref{eq:root_cond} to recursively narrow the interval until some convergence criteria is reached from an initial interval that must satisfy equation \eqref{eq:root_cond}. The search is done be dividing the interval at its midpoint $c$ and recursively search the subinterval, that satisfies equation \eqref{eq:root_cond}. As the root is desired with some tolerance $\tau$, one cannot be satisfied until the size of the interval in which the root lies has size
%
\begin{equation} \label{eq:conv_crit1}
	\Delta x \leq \tau,
\end{equation}
%
hence this is part of the convergence criteria. To make sure that a root is actually found one must obviously check that the estimate has an absolute function value smaller than some tolerance. For the sake of convinience the same tolerance as before, $\tau$, is used. That is once equation \eqref{eq:conv_crit1} is fulfilled one uses equation \eqref{eq:root_cond} to check whether the root is in the interval $[a,c]$ or $[c,b]$. Denoting the interval containing the root $[x_\mathrm{min}, x_\mathrm{max}]$ then if
%
\begin{equation}
	|f(q)| \leq \tau
\end{equation}
%
for a value $q \in \mathcal X = \{x_\mathrm{min}, x_\mathrm{max}\}$ the method is considered converged with the root estimated as the element of $\mathcal X$ with the smallest absolute function value. The error is estimated as the size of the interval containing the root
%
\begin{equation}
	\Delta x = |x_\mathrm{max}-x_\mathrm{min}|.
\end{equation}
%
The result of the first test of the implementation of the bisection can be seen in figure \ref{fig:bisection} and the rest can be seen in the file \verb|bracketing/out.txt|. The method provides an estimate of the root with an actual error smaller than the estimated error which is smaller than the desired tolerance, hence it works very well.

\begin{figure*}
	\verbatiminput{single_matrix_bracketing_test/out.txt}
	\caption{Output from the diagonalization test of the symmetric rank-1 updated matrix method using the bisection root-finding algorithm.}
	\label{fig:test_res_bi}
\end{figure*}
%
\subsection{Eigenvalue Method with Bisection}
With a working bisection method for root finding this can be used in the implementation of the symmetric rank-1 eigenvalue algorithm with the intervals of equations (\ref{eq:int_a}-\ref{eq:int_b}) as the initial interval. Running the same test as in section \ref{sec:test}, that resultet in figure \ref{fig:test_res}, for the implementation with the bisection method figure \ref{fig:test_res_bi} is obtained. Here convergence is reached, but usually with more function evaluations than Newton's method. This is expected since Newton's method is fast assuming that the provided initial value is good, which is most likely the case for the scheme described in section \ref{sec:init_val} for at matrix of small dimention as in the test. Newton's method uses line search to optimize the step size, whereas this implementation of the bisection method does not optimize the interval size -- it is the simplest and most naive bracketing method and in general quite slow. Using an optimized bracketing method would be the fastest implementation to reliably obtain eigenvalues from the symmetric rank-1 method. That is however outside the scope of this project.

\begin{figure}[t]
	\resizebox{\columnwidth}{!}{\input{Scale_t_n.tex}}
	\caption{Scaling of the number of operations for the three symmetric rank-1 method implementations and the two Jacobi methods.}
	\label{fig:scaling_tot_n}
\end{figure}
%
\begin{figure}[t]
	\resizebox{\columnwidth}{!}{\input{Scale_t_t.tex}}
	\caption{Scaling of time of computation for the three symmetric rank-1 method implementations and the Jacobi methods.}
	\label{fig:scaling_tot_t}
\end{figure}
%
\subsection{Hybrid Method}
The bisection implementation is limited in the sense that it is slow and relies on recursive calls of the same function, which will course overflow errors if the starting interval is too large. In section \ref{sec:test} it was concluded that Newton's method works but that it was a struggle to reliably obtain an initial value from which the method could find the desired root. This can in theory be solved with a hybrid approach were a few iterations of the bisection methods is used to narrow the interval in which the root is. From here Newton's method can be used to rapidly find the root, with the center of the interval as initial guess. The number of bisection iterations should be large enough to provide an initial point from which Newton's method will converge to the correct root, but as small as possible since Newton's method is faster. By trial and error it was observed that rounded to the nearest integer
%
\begin{equation}
	n_\mathrm{iter} = \begin{cases}
		n/4, \quad &n>12 \\
		3, \quad &n\leq12
	\end{cases},
\end{equation}
%
where $n$ is the matrix dimension, seemed to be at good number of bisection iterations. This was determined using plots like the one in figure \ref{fig:func_test_hybrid} that show $\mathrm{sign}[f(\lambda)]\log_{10}|f(\lambda)|$ plotted against $\lambda$ since the secular equation oscillated rapidly between large function values. This new version of the diagonalization method is tested analogously to the previous versions and the results are displayed in figure \ref{fig:test_res_hybrid}.

\subsection{Scaling of All Impementations}
Figures \ref{fig:scaling_tot_n} and \ref{fig:scaling_tot_t} show how the three implementations of the symmetric rank-1 update specific method scale with the matrix dimension in comparison to each other and also to the classic and cyclic Jacobi methods. It is noticed that the datapoints for the rank-1 methods stop earlier than the Jacobi methods, which is because the root-finding method does not converge for the matrix of higher dimension tried. The random number generator is for these plots seeded to provide consistent result, so the methods might work for certain matrices of size $n\sim20$ but obviously not all. The bisection method is too slow and reaches stack overflow and the Newton-based methods end in an endless stream of bad points, hence it does not converge. It is thus quite clear that the root-finding method is also the bottleneck that dictates the performance of the method. As seen from figure \ref{fig:func_test_hybrid} some of the eigen values are every close and thus difficult to reliably distinguish. It might be possible to improve the method by using the function
%
\begin{equation}
	\tilde f(\lambda) = \mathrm{sign}[f(\lambda)]\log_{10}|f(\lambda)|
\end{equation}
%
as the seqular equation to lessen the problem of wild oscillations. That was however not attempted in this project. Another obvious improvement would be to use an optimized bracketing method. In conclusion these result serve as a proof of concept -- the method can work and it has potential to outperform the Jacobi methods. However to realize this potential, one must optimize the implementation, and especially the root-finding part, to a greater extend than attempted here.
%
\begin{figure*}[]
	\centering
	\resizebox{.9\textwidth}{!}{\input{Test_hybrid.tex}}
	\caption{The function $\mathrm{sign}[f(\lambda)]\log_{10}|f(\lambda)|$ for a size $n=40$ matrix A on the form of equation \eqref{eq:Aform} with randomly generated matrix elements. This expression is plotted rather than the function $f(\lambda)$ itself to conserve the sign of the function while using a logarithmic scale. In the figure "Edge" denotes the point on the edge of the interval from equations (\ref{eq:int_a}-\ref{eq:int_b}) that contains the eigenvalue not enclosed by adjacent elements of the diagonal matrix $D$. In the figure $d_i$ denote the sorted matrix elements of $D$ and $\lambda_{0i}$ denote the value that will be passed to Newton's method from the bisection iterations. From this figure one notes, that the gradient of all the orange points point towards the correct eigen value in green.}
	\label{fig:func_test_hybrid}
\end{figure*}
%
\begin{figure*}
	\verbatiminput{single_matrix_hybrid_test/out.txt}
	\caption{Output from the diagonalization test of the symmetric rank-1 updated matrix method using the hybrid root-finding algorithm.}
	\label{fig:test_res_hybrid}
\end{figure*}
%

\newpage
\onecolumn
\bibliographystyle{unsrt}
\bibliography{ppnm}

\end{document}
