using System;
using System.IO;
using System.Collections.Generic;
using static System.Math;
using static System.Console;
public class main{
public static void Main(){
	int n = 5;
	var rnd = new Random();
	matrix A = new matrix(n,n);
	vector e_rank1 = new vector(n);
	vector e_jacobi = new vector(n);
	vector u = new vector(n);
	matrix V = new matrix(n,n);
	for(int i=0;i<n;i++){
		A[i,i] = 10*(rnd.NextDouble() - .5);
		u[i] = 2*(rnd.NextDouble() - .5);
	}//for
	double c = 2*(rnd.NextDouble() -.5);
	Write("Eigenvalue Decomposition Test\n");
	matrix B = A + c*matrix.outer(u,u);
	A.print("Random Diagonal Matrix (D): \n");
	u.print("Random Update Vector (u): \n");
	Write($"Random Number c: {c}\n");
	Write($"Norm of u: {u.norm()}\n");
	Write($"c*|u|: {c*u.norm()}\n");
	B.print("Symmetric Rank-1 Updated Matrix (A=D+c*u*u^T): \n");
	int n0 = 0;
	double acc = 1e-3;
	int sweeps = jacobi.cyclic(B,e_jacobi,V);
	int nop = eig.rank1_bisection(A,u,e_rank1,c:c,n0:n0,n:n,acc:acc);
	Write("\n");
	e_rank1.print("Rank-1 Method Eigenvalues: ","{0,10:g4} ");
	e_jacobi.print("Jacobi Method Eigenvalues: ","{0,10:g4} ");
	Write($"\nNumber of Symmetric Rank-1 Method Operations: {nop}\n");
	Write($"Number of Jacobi Operations: {sweeps*n}\n\n");
	if(vector.approx(e_rank1,e_jacobi,acc)){
		Write($"The two vectors of eigenvalues are equal to absolute accuracy acc = {acc}\n");
		Write("TEST PASSED!\n");
	}//for
	else{
		Write($"The two vectors of eigenvalues are not equal to absolute accuracy acc = {acc}\n");
		(e_rank1-e_jacobi).print("Difference in the eigenvalues of the two methods: \n");
		Write("TEST FAILED!\n");
	}//else
}//Main
}//main
