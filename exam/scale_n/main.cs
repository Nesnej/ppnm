using System;
using System.Diagnostics;
using static System.Math;
using static System.Console;
public class main{
public static void Main(){
	int n0=3, nmax = 20;
	// It apparently takes longer to run a method in a class the first times,
	// thus a dummy run on an identity matrix is done to avoid this problem.
	matrix Q = new matrix(n0,n0); Q.setid();
	vector eigval = new vector(n0);
	eig.rank1_newton(Q,eigval,eigval,c:0);
	var watch=new Stopwatch();
	watch.Reset();
	watch.Start();
	watch.Stop();
	double acc = 1e-3, trank1=0;
	int nr=0;
	for(int n = n0;n<=nmax;n++){
		var rnd = new Random(42);
		matrix D = new matrix(n,n);
		vector e = new vector(n);
		vector u = new vector(n);
		for(int i=0;i<n;i++){
			D[i,i] = 10*(rnd.NextDouble() - .5);
			u[i] = 2*(rnd.NextDouble() - .5);
		}//for
		double c = 2*(rnd.NextDouble() -.5);
		watch.Reset();
		watch.Start();
		int N = eig.rank1_newton(D,u,e,c:c,n0:0,n:n,acc:acc);
		watch.Stop();
		Write($"{n} {N} {watch.ElapsedTicks}\n");
		if(n==nmax) trank1 = watch.ElapsedTicks;
		if(n==n0) nr = N;
	}//for
	Write("\n\n");
	for(double x=n0;x<=nmax;x+=1D/32)
		Write($"{x} {x*x*nr/n0/n0} {x*x*trank1/nmax/nmax}\n");
}//Main
}//main
