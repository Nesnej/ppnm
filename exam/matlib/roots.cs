using System;
using System.Diagnostics;
using static System.Math;
using static System.Double;
using static System.Console;
public class roots{
public static vector newton(Func<vector,vector> f, vector x,
		double acc=1e-3, double dx=1e-7){
	int n=x.size, niter=0;
	double lambda_min = 1D/64;
	// Setting up gradient and point for function evaluation
	matrix J = new matrix(n,n);
	vector root = x.copy();
	vector fr = f(root);
	vector z,fz;
	// Do Newton step until convergence
	while(fr.norm()>acc){
		if(niter>100){
			Error.Write($"The number of iterations exceeded the maximum allowed at {niter}\n");
			break;
		}//if
		// Calculating gradient
		for(int j=0;j<n;j++){
			root[j] += dx;
			vector df = f(root)-fr;
			for(int i=0;i<n;i++) J[i,j] = df[i]/dx;
			root[j] -= dx;
		}//for
		// Calculating Newton full step
		var qr = new gramschmidt(J);
		var dr = qr.solve(-1*fr);
		double lambda = 1D;
		z = root+lambda*dr;
		fz = f(z);
		// Doing modified Newton step
		while(fz.norm()>(1-lambda/2)*fr.norm()){
			if(lambda>lambda_min){
				lambda /= 2;
				fz = f(root+lambda*dr);
			}//if
			// Breaking if too many step attempts are bad
			else {Error.Write($"bad point: lambda={lambda_min}\n"); break;}
		}//while
		// Updating root estimate
		root += lambda*dr;
		fr = f(root);
		niter++;
	}//while
	return root;
}//newton

public static double newton(Func<double,double> f, double x,
		double acc=1e-3, double dx=1e-7){
	double lambda_min = 1D/64;
	// Setting up derivative and point for function evaluation
	double fx = f(x);
	double z,fz,J;
	// Do Newton step until convergence
	while(Abs(fx)>acc){
		// Calculating derivative
		J = (f(x+dx)-fx)/dx;
		// Calculating Newton full step
		double lambda = 1D;
		z = x-lambda*fx/J;
		fz = f(z);
		// Doing modified Newton step
		while(Abs(fz)>(1-lambda/2)*Abs(fx)){
			if(lambda>lambda_min){
				lambda /= 2;
				fz = f(x-lambda*fx/J);
			}//if
			// Breaking if too many step attempts are bad
			else {Error.Write($"bad point: lambda={lambda_min}\n"); break;}
		}//while
		// Updating root estimate
		x -= lambda*fx/J;
		fx = f(x);
	}//while
	return x;
}//newton

public static double[] bisection(Func<double,double> f, double a, double b,
		double acc=1e-3, double fa=NaN, double fb=NaN){
	// Setting the function values at the interval end points if they are not provided
	if(IsNaN(fa)) fa = f(a);
	if(IsNaN(fb)) fb = f(b);
	Trace.Assert(a!=b,"The starting interval must contain more than one point");
	Trace.Assert(a<b,"The starting interval must be ordered with the first point smaller than the last");
	Trace.Assert(Sign(fa)!=Sign(fb),"The sign of the function evaluated at the endpoints must be different");
	// Midpoint and function value at the midpoint
	double c=(b+a)/2, fc=f(c);
	// Half interval size
	double dx=(b-a)/2;
	// If the interval size is smaller than tolerance there is an interval of size
	// dx smaller than tolerance, in which the root is
	if(dx<=acc){
		// If a and c have opposite signs the root is in [a,c]
		if(Sign(fa)!=Sign(fc)){
			// Is f(a) is smaller than tolerance?
			if(Abs(fa)<acc){
				// Is c a better estimate than a?
				if(Abs(fc)<Abs(fa)) return new double[] {c,dx};
				// If not return a
				else return new double[] {a,dx};
			}//if
		}//if
		// If a and c have the same sign the root is in [c,b]
		else{
			// Is f(b) is smaller than tolerance?
			if(Abs(fb)<acc){
				// Is c a better estimate than b?
				if(Abs(fc)<Abs(fb)) return new double[] {c,dx};
				// If not return b
				else return new double[] {b,dx};
			}//if
		}//else
	}//if
	// If not converged continue recursively on the interval with function values
	// of opposite signs and the edges
	if(Sign(fa)==Sign(fc)) return bisection(f,c,b,acc:acc,fa:fc,fb:fb);
	else return bisection(f,a,c,acc:acc,fa:fa,fb:fc);
}//bisection

public static double bisection_newton(Func<double,double> f, double a, double b, double acc=1e-3,
		int nmax=5, double fa=NaN, double fb=NaN, int niter=0, bool log=false){
	// Setting the function values at the interval end points if they are not provided
	if(IsNaN(fa)) fa = f(a);
	if(IsNaN(fb)) fb = f(b);
	Trace.Assert(a!=b,"The starting interval must contain more than one point");
	Trace.Assert(a<b,"The starting interval must be ordered with the first point smaller than the last");
	Trace.Assert(Sign(fa)!=Sign(fb),"The sign of the function evaluated at the endpoints must be different");
	// Midpoint and function value at the midpoint
	double c=(b+a)/2, fc=f(c);
	// When the maximum number of bisection iterattions is reached, proceed with Newton's method
	if(niter>=nmax){
		if(log) Console.Error.Write($"Starting Newton's Method at: x0={c}, f(x0)={fc}\n");
		return newton(f,c,acc:acc,dx:acc*1e-3);
	}//if
	// Half interval size
	double dx=(b-a)/2;
	// If the interval size is smaller than tolerance there is an interval of size
	// dx smaller than tolerance, in which the root is.
	if(dx<=acc){
		// If a and c have opposite signs the root is in [a,c]
		if(Sign(fa)!=Sign(fc)){
			// Is f(a) is smaller than tolerance?
			if(Abs(fa)<acc){
				// Is c a better estimate than a?
				if(Abs(fc)<Abs(fa)) return c;
				// If not return a
				else return a;
			}//if
		}//if
		// If a and c have the same sign the root is in [c,b]
		else{
			// Is f(b) is smaller than tolerance?
			if(Abs(fb)<acc){
				// Is c a better estimate than b?
				if(Abs(fc)<Abs(fb)) return c;
				// If not return b
				else return b;
			}//if
		}//else
	}//if
	// If not converged continue recursively on the interval with function values
	// of opposite signs and the edges
	if(Sign(fa)==Sign(fc)) return bisection_newton(f,c,b,nmax:nmax,acc:acc,fa:fc,fb:fb,niter:niter+1,log:log);
	else return bisection_newton(f,a,c,nmax:nmax,acc:acc,fa:fa,fb:fc,niter:niter+1,log:log);
}//bisection
}//root
