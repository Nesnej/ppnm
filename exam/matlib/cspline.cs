using System;
using static System.Console;
public class cspline{
	double[] x,y,b,c,d;

// Binary search method
public static int bin_search(double[] x, double z){
	int i = 0;
	int j = x.Length-1;
	while(j-i>1){
		int m = (i+j)/2;
		if(z>x[m]){i=m;}
		else{j=m;}
	}//while
	return i;
}//binsearch

public cspline(double[] xs, double[] ys){
	int n = xs.Length;
	// Error if number of x and y values are different
	System.Diagnostics.Trace.Assert(ys.Length>=n);
	x = new double[n];
	y = new double[n];
	b = new double[n];
	c = new double[n-1];
	d = new double[n-1];
	for(int i=0;i<n;i++){x[i]=xs[i];y[i]=ys[i];}
	var D = new double[n];
	var Q = new double[n-1];
	var B = new double[n];
	var h = new double[n-1];
	var p = new double[n-1];
	for(int i=0;i<n-1;i++){
		h[i] = x[i+1] - x[i];
		p[i] = (y[i+1] - y[i])/h[i];
	}//for
	D[0] = 2; D[n-1] = 2;
	Q[0] = 1;
	B[0] = 3*p[0]; B[n-1] = 3*p[n-2];
	// Building the tridiagonal system
	for(int i=0;i<n-2;i++){
		Q[i+1] = h[i]/h[i+1];
		D[i+1] = 2*Q[i+1]+2;
		B[i+1] = 3*(p[i] + p[i+1]*Q[i+1]);
	}//for
	// Gaussian elimination
	for(int i=1;i<n;i++){
		D[i] -= Q[i-1]/D[i-1];
		B[i] -= B[i-1]/D[i-1];
	}//for
	// Back-substitution
	b[n-1] = B[n-1]/D[n-1];
	for(int i=n-2;i>=0;i--){
		b[i] = (B[i] - Q[i]*b[i+1])/D[i];
	}//for
	// constructing the spline coefficients
	for(int i=0;i<n-1;i++){
		c[i] = (3*p[i] - b[i+1] - 2*b[i])/h[i];
		d[i] = (b[i] + b[i+1] - 2*p[i])/(h[i]*h[i]);
	}//for
}//qspline - constructor

public double eval(double z){
	// Error if z is outside interpolated interval
	System.Diagnostics.Trace.Assert(z>=x[0] && z<=x[x.Length-1]);
	int i = bin_search(x,z);
	double dx = z-x[i];
	return y[i] + dx*b[i] + c[i]*dx*dx + d[i]*dx*dx*dx;
}//eval

public double integ(double z){
	int i = bin_search(x,z);
	double sum = 0;
	double dx;
	for(int j=0;j<i;j++){
		dx = x[j+1] - x[j];
		sum += dx*(y[j] + dx*(b[j]/2 + dx*(c[j]/3 + dx*d[j]/4)));
	}//for
	dx = z - x[i];
	return sum += dx*(y[i] + dx*(b[i]/2 + dx*(c[i]/3 + dx*d[i]/4)));
}//integ

public double deriv(double z){
	int i = bin_search(x,z);
	return b[i] + 2*c[i]*(z-x[i]) + 3*d[i]*(z-x[i])*(z-x[i]);
}//deriv
}//cspline
