using System;
using static System.Math;
public class lsfit{
public readonly vector c;
public readonly Func<double,double>[] f;
public readonly int nf;
public readonly matrix sigma;
public lsfit(vector x, vector y, vector dy, Func<double,double>[] fs){
	f = fs;
	nf = fs.Length;
	int n = x.size, m = fs.Length;
	matrix A = new matrix(n,m);
	vector b = new vector(n);
	// Setting up the system of linear equations
	for(int i=0;i<n;i++){
		b[i] = y[i]/dy[i];
		for(int j=0;j<m;j++){
			A[i,j] = f[j](x[i])/dy[i];
		}//for
	}//for
	// Solving the linear equations
	var qr = new gramschmidt(A);
	c = qr.solve(b);
	var mat = new gramschmidt(A.T*A);
	sigma = mat.inv();
}//constuctor

public double eval(double x){
	double F = 0;
	for(int i=0;i<nf;i++) F += c[i]*f[i](x);
	return F;
	//for
}//eval
}//lsfit
