using System;
using System.Collections.Generic;
using static System.Console;
using static System.Math;
public class mini{
// Square root of machine epsilon on my computer - see the directory mach_eps in the minimization excercise A directory.
public static readonly double EPS = Sqrt(1.11022302462516E-16);

public static int qnewton_sr1(Func<vector,double> f,
		ref vector x, double acc=1e-3,
		int lim=999, bool log=false){
	// Setting up the method parameters
	int nsteps=0,n=x.size;
	double lambda_min = 1D/64, alpha=1e-4;
	// Defining gadient and inverse Hessian
	vector g = gradient(f,x);
	matrix B = new matrix(n,n); B.setid();
	// Calculate and perform modified Newton step undtil convergence
	while(g.norm()>acc){
		vector dx = -B*g;
		// Break if the step size becomes too small
		if(dx.norm()<EPS*x.norm()) break;
		double lambda = 1D;
		double fx = f(x);
		double fz = f(x+lambda*dx);
		// Convergence criteria
		double armijo = fx+alpha*lambda*(dx%g);
		while(fz>armijo){
			//Calculating mModified Newton step
			if(lambda>1D/64){
				lambda /= 2;
				fz = f(x+lambda*dx);
			}//if
			else{
				// Resetting inverse Hessian if the the point is bad
				B.setid();
				fz = f(x+lambda*dx);
				if(log){
					Error.Write($"bad point: lambda={lambda_min}\n");
					x.eprint("current point ");
				}//if
				break;
			}//else
		}//while
		// Performing modified Newton step
		x += lambda*dx;
		// Symmetric rank-1 inverse Hessian update
		vector y = gradient(f,x)-g;
		vector u = lambda*dx - B*y;
		if(Abs(u%y)>10*EPS) B.update(u,u,1D/(u%y));
		else B.setid();
		g = gradient(f,x);
		nsteps++;
		// Breaking if maximum number of steps is reached
		if(nsteps==lim){
			if(log){
				Error.Write($"Maximum number of steps reached. Exiting at:\n");
				x.eprint();
				Error.Write("\n");
			}//if
			break;
		}//if
	}//while
	return nsteps;
}//qnewton

public static int simplex(Func<vector,double> f, ref vector x,
		double acc=1e-3, double dx = 1e-2, int lim=999){
	int nsteps=0, n=x.size, N=n+1;
	// List of simplex points
	List<vector> p = new List<vector>();
	// Adding starting point to simplex
	p.Add(x.copy());
	// Adding points with one coordinate displaced by dx to the simplex
	for(int i=0;i<n;i++){
		x[i] += dx;
		p.Add(x.copy());
		x[i] -= dx;
	}//for
	double dist=0;
	// Calculating the sum of distances between points
	for(int i=0;i<N;i++) dist += (p[(i+1)%N]-p[i]).norm();
	vector fs = new vector(N);
	// Calculating function values for each simplex point
	for(int i=0;i<N;i++) fs[i] = f(p[i]);
	int imin,imax;
	// Move the simplex as long as the average distance between points exceeds the accurary goal
	while(dist/n>acc){
		// Finding the simplex points with the smallest and largest function values
		imax = max_index(fs);
		vector phi = p[imax];
		imin = min_index(fs);
		vector pl = p[imin];
		// Calculating centroid
		vector pc = cent(p,imax);
		// Storing reflected point and function value
		vector pref = 2*pc-phi;
		double fref = f(pref);
		// Checking if the reflected point function value is smaller than the smallest in the simplex
		if(fref < f(pl)){
			// Try expansion
			if(f(3*pc-2*phi) < fref) p[imax] = 3*pc-2*phi;
			// Otherwise accept reflection
			else p[imax] = pref;
			// Store the function value of the new simplex point
			fs[imax] = f(p[imax]);
		}//if
		else{
			// If the reflected point is better than the higest simplex point then accept it
			if(fref < f(phi)){
				p[imax] = pref;
				// Store the function value of the new simplex point
				fs[imax] = f(p[imax]);
			}//if
			else{
				// Try contraction
				if(f((phi+pc)/2) < fref){
					p[imax] = (phi+pc)/2;
					// Store the function value of the new simplex point
					fs[imax] = f(p[imax]);
				}//if
				else{
					// Perform reduction of the simplex
					for(int i=0;i<imin;i++){
						p[i] = (p[i] + pl)/2;
						fs[i] = f(p[i]);
					}//for
					for(int i=imin+1;i<n+1;i++){
						p[i] = (p[i] + pl)/2;
						fs[i] = f(p[i]);
					}//for
				}//else
			}//else
		}//else
		// Recalculate the sum of distances between points
		dist=0;
		for(int i=0;i<N;i++) dist += (p[(i+1)%N]-p[i]).norm();
		// If the number of simplex steps exceeds the limit break the loop
		nsteps++;
		if(nsteps==lim){
			Error.Write("Maximum number of steps reached\n");
			break;
		}//if
	}//while
	// Use the simplex point of smallest function value as estimate of the minimum
	imin = min_index(fs);
	x = p[imin];
	return nsteps;
}//simplex

// Helper methods for the two minimization algoritms
// Function that calculates the gradient of a function in a point
public static vector gradient(Func<vector,double> f, vector x){
	vector g = new vector(x.size);
	double dx, fx = f(x);
	for(int i=0;i<x.size;i++){
		// Setting the stepsize no smaller than machine epsison
		if(Abs(x[i])<Sqrt(EPS)) dx = EPS;
		else dx = Abs(x[i])*EPS;
		x[i] += dx;
		g[i] = (f(x)-fx)/dx;
		x[i] -= dx;
	}//for
	return g;
}//gradient

// Method for calculating the centroid of all points but the one with index imax
public static vector cent(List<vector> p, int imax){
	int n = p.Count-1;
	vector pc = new vector(n);
	for(int i=0;i<n+1;i++) if(i!=imax) pc += p[i];
	return pc/n;
}//cent

// Function for finding the index of the largest entry
public static int max_index(vector fs){
	int j=0;
	for(int i=1;i<fs.size;i++) if(fs[i]>fs[j]) j=i;
	return j;
}//max_index

// Function for finding the index of the smallest entry
public static int min_index(vector fs){
	int j=0;
	for(int i=1;i<fs.size;i++) if(fs[i]<fs[j]) j=i;
	return j;
}//min_index
}//mini
