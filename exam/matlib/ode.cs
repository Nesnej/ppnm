using System;
using static System.Math;
using static System.Console;
using System.Collections.Generic;
public partial class ode{
// Function that calls the driver with the RK1(2) stepper
public static vector rk12(Func<double,vector,vector> F, double a, vector ya,
		double b, double acc=1e-3, double eps=1e-3, double h=.1,
		List<double> xlist=null, List<vector> ylist=null, int lim=999){
	return driver(F, a, ya, b, acc, eps, h, xlist, ylist, lim, rkstep12);
}//rk12

// Function that calls the driver with the RK4(5) stepper
public static vector rk45(Func<double,vector,vector> F, double a, vector ya,
		double b, double acc=1e-3, double eps=1e-3, double h=.1,
		List<double> xlist=null, List<vector> ylist=null, int lim=999){
	return driver(F, a, ya, b, acc, eps, h, xlist, ylist, lim, rkstep45);
}//rk12

public static vector driver(Func<double,vector,vector> F, double a, vector ya, double b,
		double acc, double eps, double h, List<double> xlist, List<vector> ylist,
		int lim, Func<Func<double,vector,vector>, double, vector, double, vector[]> stepper) {
	int nstep = 0;
	// Reserve a for the initial point and use x for the continually updating point
	double x = a;
	vector yx = ya;
	// If lists are not initialized initialize them with the initial point
	if(xlist!=null) {xlist.Clear(); xlist.Add(a);}
	if(ylist!=null) {ylist.Clear(); ylist.Add(ya);}
	// Continue driving until the endpoint is reached
	while(x<b){
		// Terminate if maximum number of steps is exceeded
		if(nstep>lim) {Error.Write($"ode driver: number of steps exceeded the limit, {lim}.\n"); return yx;}
		// Bound the step within the closed interval [a,b]
		if(x+h>b) h=b-x;
		// Try a step of size h
		vector[] trial = stepper(F,x,yx,h);
		vector yh = trial[0]; vector err = trial[1];
		// Calculate tolerance
		vector tol = new vector(err.size);
		for(int i=0;i<tol.size; i++){
			//tol[i] = Max(acc,Abs(eps*yh[i]))*Sqrt(h/(b-a));
			tol[i] = (acc+eps*Abs(yh[i]))*Sqrt(h/(b-a));
			if(err[i]==0) err[i] = tol[i]/4;
		}//for
		// Finding the smallest tolerance to error ratio
		double err_fac = Abs(tol[0]/err[0]);
		bool crit = true;
		for(int i=1; i<tol.size; i++){
			err_fac = Min(err_fac,Abs(tol[i]/err[i]));
			// If the estimated error exceeds tolerance the step is bad and the step
			// acceptance criterion is set to false. If the error is too large
			// there is no reason to search the entire error vector and the loop is
			// stoped by setting i=tol.size
			if(Abs(err[i])>tol[i]){
				crit=false;
				i=tol.size;
			}//if
		}//for
		// If the estimated error is smaller than tolerance accept the step
		if(crit){
			x += h; yx = yh; nstep++;
			if(xlist!=null) xlist.Add(x);
			if(ylist!=null) ylist.Add(yx);
		}//if
		else{
			Error.Write($"driver: bad step at {x}\n");
		}//else
		// Determine next step size
//		h *= Min(Pow(err_fac,.25)*.95,2);
		h *= Pow(err_fac,.25)*.95;
	}//while
	return yx;
}//driver

static vector[] rkstep12(Func<double,vector,vector> f, double t, vector yt, double h){
	// Derivative vector at the point t
	vector k0 = f(t,yt);
	// Derivative vector at the midpoint
	vector k = f(t+.5*h,yt+.5*h*k0);
	// Midpoint step
	vector yh = yt + h*k;
	// Error as difference in midpoint and Euler step
	vector err = (k-k0)*h;
	vector[] res = {yh,err};
	return res;
}//rkstep12

static vector[] rkstep45(Func<double,vector,vector> f, double t, vector yt, double h){
	// Derivative vectors at certain points according to the Runge-Kutta Fehlberg
	// Butcher's tableau on the Wikipedia page
	vector k1 = h*f(t,yt);
	vector k2 = h*f(t + .25*h, yt + .25*k1);
	vector k3 = h*f(t + 3D/8*h, yt + 3D/32*k1 + 9D/32*k2);
	vector k4 = h*f(t + 12D/13*h, yt + 1932D/2197*k1 - 7200D/2197*k2 + 7296D/2197*k3);
	vector k5 = h*f(t + h, yt + 439D/216*k1 - 8*k2 + 3680D/513*k3 - 845D/4104*k4);
	vector k6 = h*f(t + .5*h, yt - 8D/27*k1 + 2*k2 - 3544D/2565*k3 + 1859D/4104*k4 - 11D/40*k5);
	vector k = 16D/135*k1 + 6656D/12825*k3 + 28561D/56430*k4 - 9D/50*k5 + 2D/55*k6;
	// RKF4(5) step
	vector yh = yt + k;
	// RKF4(5) error estimate
	vector err = k - (25D/216*k1 + 1408D/2565*k3 + 2197D/4104*k4 - 1D/5*k5);
	vector[] res = {yh,err};
	return res;
}//rkstep45

}//ode
