using System;
using static System.Console;
public class qspline{
	double[] x,y,p,c;

// Binary search method
public static int bin_search(double[] x, double z){
	int i = 0;
	int j = x.Length-1;
	while(j-i>1){
		int m = (i+j)/2;
		if(z>x[m]){i=m;}
		else{j=m;}
	}//while
	return i;
}//binsearch

public qspline(double[] xs, double[] ys){
	int n = xs.Length;
	// Error if the number of x and y values are different
	System.Diagnostics.Trace.Assert(ys.Length>=n);
	x = new double[n];
	y = new double[n];
	for(int i=0;i<n;i++){x[i]=xs[i];y[i]=ys[i];}
	p = new double[n-1];
	for(int i=0;i<n-1;i++){
		p[i] = (y[i+1] - y[i])/(x[i+1] - x[i]);
	}//for
	c = new double[n-1];
	c[0] = 0;
	for(int i=1;i<n-1;i++){
		c[i] = (p[i] - p[i-1] - c[i-1]*(x[i]-x[i-1]))/(x[i+1] - x[i]);
	}//for
	c[n-2] /= 2;
	for(int i=n-3;i>=0;i--){
		c[i] = (p[i+1] - p[i] - c[i+1]*(x[i+2]-x[i+1]))/(x[i+1] - x[i]);
	}//for
}//qspline - constructor

public double eval(double z){
	// Error if attempted evaluation outside interpolated interval
	System.Diagnostics.Trace.Assert(z>=x[0] && z<=x[x.Length-1]);
	int i = bin_search(x,z);
	double dx = z-x[i];
	return y[i] + dx*p[i] + c[i]*dx*(z-x[i+1]);
}//eval

public double integ(double z){
	int i = bin_search(x,z);
	double sum = 0;
	double dx,b;
	for(int j=0;j<i;j++){
		dx = x[j+1] - x[j];
		b = p[j] - c[j]*dx;
		sum += dx*(c[j]*dx*dx/3 + b*dx/2 + y[j]);
	}//for
	dx = z - x[i];
	b = p[i] - c[i]*(x[i+1]-x[i]);
	return sum += dx*(c[i]*dx*dx/3 + b*dx/2 + y[i]);
}//integ

public double deriv(double z){
	int i = bin_search(x,z);
	return p[i]-c[i]*(x[i+1]-x[i]) + 2*c[i]*(z-x[i]);
}//deriv
}//cspline
