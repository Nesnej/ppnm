public class sort{
// Sorting algorithm from https://en.wikipedia.org/wiki/Quicksort
public static void quicksort(vector a, int lo, int hi){
	if(lo<hi){
		int p = partition(a,lo,hi);
		quicksort(a,lo,p-1);
		quicksort(a,p+1,hi);
	}//if
}//quicksort

public static int partition(vector a, int lo, int hi){
	double pivot = a[hi];
	int i = lo;
	double ai = a[i];
	for(int j=lo;j<hi;j++){
		if(a[j]<pivot){
			double aj = a[j];
			ai = a[i];
			a[i] = aj;
			a[j] = ai;
			i++;
		}//if
	}//for
	double ah = a[hi];
	ai = a[i];
	a[i] = ah;
	a[hi] = ai;
	return i;
}//partition
}//sort
