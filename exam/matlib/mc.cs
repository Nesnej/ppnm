using System;
using System.IO;
using System.Diagnostics;
using System.Collections.Generic;
using static System.Math;
using static System.Double;
using static System.Console;
public static class mc{
	
// Random number generator for the method	
public static Random rnd = new Random();

// Function for calculating a new point
public static vector newx(vector a, vector dx){
	vector x = a.copy();
	for(int i=0;i<x.size;i++) x[i] += rnd.NextDouble()*dx[i];
	return x;
}//newx

public static double[] plain(Func<vector,double> f, vector a, vector b, int N){
	// Storing the vector between the integration limits
	vector dx = b-a;
	int n = a.size;
	// Check that the size of the limit vectors are equal
	Trace.Assert(n==b.size,"The dimension of a and b are different");
	// Calculate the volume of integration
	double vol = 1;
	for(int i=0;i<n;i++) vol *= dx[i];
	// Calculate the sum of function values and their squares
	double sum=0,sum2=0;
	for(int i=0;i<N;i++){
		vector x = newx(a,dx);
		double fx = f(x);
		sum += fx;
		sum2 += fx*fx;
	}//for
	// Calculate mean function value and standard deviation
	double mean = sum/N;
	double sigma = Sqrt((sum2/N-mean*mean)/N);
	// Return integral and error estimate
	return new double[] {mean*vol, sigma*vol};
}//plain

public static double[] rss(Func<vector,double> f, vector a, vector b,
		int N, int Nmax, double acc=.1, double vol=NaN,
		vector oldstat=null, int oldN=0,
		List<vector> xs=null, List<double> fs=null){
	// Storing the vector between the integration limits
	vector dx = b-a;
	// Adjust the number of new points if it exceeds limit
	if(N>Nmax-oldN) N=Nmax-oldN;
	int n = a.size;
	// Check that the size of the limit vectors are equal
	Trace.Assert(n==b.size,"The dimension of a and b are different");
	// Calculate the volume of integration and initialize parameters
	if(IsNaN(vol)){
		vol = 1;
		for(int i=0;i<n;i++) vol *= dx[i];
		// Add points to the lists for use in later iterations
		xs = new List<vector>();
		fs = new List<double>();
		oldstat = new vector(0,0,0);
	}//if
	double sum=0,sum2=0;
	for(int i=0;i<N;i++){
		vector x = newx(a,dx);
		double fx = f(x);
		xs.Add(x);
		fs.Add(fx);
		sum += fx;
		sum2 += fx*fx;
	}//for
	// Calculate the sum of function values and their squares
	double mean = sum/N;
	double sig = Sqrt(sum2/N-mean*mean);
	// Integral and error estimate
	double integral = vol*(mean*N+oldstat[0]*oldN)/(N+oldN);
	double err = vol*Sqrt(sig*N+oldstat[1]*oldN)/(N+oldN);
	// Check for convergence
	if(err<acc){
		return new double[] {integral, err, N+oldN};
	}//if
	// If the number of points exceed the limit break the recursive call
	else if(N+oldN>=Nmax) return new double[] {integral, err, N+oldN};
	else{
		// Initialize variables for stratification
		double vmax = -1;
		int kmax = 0, k=0;
		double[] oldstat1=new double[3];
		double[] oldstat2=new double[3];
		List<vector> xs1=new List<vector>();
		List<vector> xs2=new List<vector>();
		List<double> fs1= new List<double>();
		List<double> fs2= new List<double>();
		// Divide the integration space in the middle for each dimension and find the dimension
		// with the largest difference in integral estimate
		for(int i=0;i<n;i++){
			sum = 0; sum2 = 0;
			double sum3 = 0, sum4 = 0;
			k = 0;
			xs1.Clear(); xs2.Clear();
			fs1.Clear(); fs2.Clear();
			// Divide points from all iterations in the two groups
			for(int j=0;j<xs.Count;j++){
				if(xs[j][i]<(a[i]+b[i])/2){
					sum += fs[j];
					sum2 += fs[j]*fs[j];
					xs1.Add(xs[j]);
					fs1.Add(fs[j]);
					k++;
				}//if
				else{
					sum3 += fs[j];
					sum4 += fs[j]*fs[j];
					xs2.Add(xs[j]);
					fs2.Add(fs[j]);
				}//else
			}//for
			// Calculate the mean function value and standard diviation for each group
			double[] stat1 = stats(sum,sum2,k);
			double[] stat2 = stats(sum3,sum4,xs.Count-k);
			// If the absolute difference in mean values is larger than the previous largest
			// set the data for this dimension as the largest
			double v = Abs(stat1[1]-stat2[1]);
			if(v>vmax){
				vmax=v; kmax=i;
				oldstat1 = stat1;
				oldstat2 = stat2;
			}//if
		}//for
		// Calculate parameters for recursion
		int kp = xs.Count - k;
		vector a2 = a.copy(), b2 = b.copy();
		a2[kmax] += dx[kmax]/2;
		b2[kmax] -= dx[kmax]/2;
		double[] res1, res2;
		int N1 = Convert.ToInt32(Nmax*oldstat1[1]/(oldstat1[1]+oldstat2[1])), N2 = Nmax-N1;
		// If there is points available do a recursive call
		if(N1-k>0){
			res1 = rss(f,a,b2,N,N1,acc/Sqrt(2),vol/2,
					oldstat1, k,
					xs1, fs1); 
		}//if
		// Otherwise return result
		else res1 = new double[] {vol*Sqrt(oldstat1[0]), vol*Sqrt(oldstat1[1]), k};
		// If there is points available do a recursive call
		if(N2-kp>0){
			res2 = rss(f,a2,b,N,N2,acc/Sqrt(2),vol/2,
					oldstat2, kp,
					xs2, fs2); 
		}//if
		// Otherwise return result
		else res2 = new double[] {vol*Sqrt(oldstat2[0]), vol*Sqrt(oldstat2[1]), kp};
		// Return the results from the two parts
		return new double[] {res1[0]+res2[0],
			Sqrt(res1[1]*res1[1]+res2[1]*res2[1]),
					res1[2]+res2[2]};
	}//else
}//rss

public static double[] stats(double sum, double sum2, int N){
	double mean = sum/N;
	double sig = Sqrt(sum2/N-mean*mean);
	return new double[] {mean, sig};
}//stats

}//mc
