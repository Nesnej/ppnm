using System;
using System.Collections.Generic;
using static System.Console;
public class main{
public static void Main(){
	int n = 5;
	// The random number generator is seeded, such that the same plot is produced
	// each time make is run. This is done to make sure that the comments on the plot
	// in ms.pdf are appropriate
	var rnd = new Random(1);
	matrix A = new matrix(n,n);
	vector e = new vector(n);
	vector u = new vector(n);
	matrix V = new matrix(n,n);
	for(int i=0;i<n;i++){
		A[i,i] = 10*(rnd.NextDouble() - .5);
		u[i] = 2*(rnd.NextDouble() - .5);
	}//for
	double c = 2*(rnd.NextDouble() -.5);
	matrix B = A + c*matrix.outer(u,u);
	double acc = 1e-3;
	jacobi.cyclic(B,e,V);
	List<int> m = new List<int>();
	for(int i=0;i<n;i++) if(!cmath.approx(u[i],0,acc:acc)) m.Add(i);
	Func<double,double> sec_eq = delegate(double x){
		double sum = 1;
		foreach(int i in m) sum *= A[i,i]-x;
		foreach(int i in m){
			double aux = c*u[i]*u[i];
			foreach(int j in m){
				if(i!=j) aux *= A[j,j]-x;
			}//foreach
			sum += aux;
		}//foreach
		return sum;
	};//sec_eq
	for(double x=e[0]-.5;x<e[n-1]+.5;x+=1D/64)
		Write($"{x} {sec_eq(x)}\n");
	Write("\n\n");
	for(int i=0;i<n;i++) Write($"{e[i]} {0}\n");
	Write("\n\n");
	for(int i=0;i<n;i++)
		Write($"{A[i,i]} {sec_eq(A[i,i])}\n");
	Error.Write($"c*|u|+d_1 = {c*u.norm()+A[n-1,n-1]}\n");
}//Main
}//main
