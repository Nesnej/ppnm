Eigenvalue Decomposition Test
Random Diagonal Matrix (D): 

     -4.75          0          0          0          0 
         0      -4.01          0          0          0 
         0          0      -3.39          0          0 
         0          0          0       1.74          0 
         0          0          0          0       2.57 
Random Update Vector (u): 
    -0.745       0.77      0.141     -0.254     -0.173 
Random Number c: 0.506271563240453
Norm of u: 1.12363900153451
c*|u|: 0.568866473824817
Symmetric Rank-1 Updated Matrix (A=D+c*u*u^T): 

     -4.47     -0.291    -0.0531      0.096     0.0652 
    -0.291      -3.71     0.0548    -0.0991    -0.0673 
   -0.0531     0.0548      -3.38    -0.0181    -0.0123 
     0.096    -0.0991    -0.0181       1.77     0.0222 
    0.0652    -0.0673    -0.0123     0.0222       2.59 

Eigenvalues of D:
d:      -4.75      -4.01      -3.39       1.74       2.57 

Rank-1 Method Eigenvalues:     -4.572     -3.631     -3.363      1.777      2.588 
Jacobi Method Eigenvalues:     -4.572     -3.631     -3.363      1.777      2.588 

Number of Symmetric Rank-1 Method Operations: 74
Number of Jacobi Operations: 150

The two vectors of eigenvalues are equal to absolute accuracy acc = 0.001
TEST PASSED!
