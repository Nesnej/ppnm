using static System.Math;
public static partial class math{

public static double erf(double x){
// single precision error function (Abramowitz and Stegun)
	if(x<0) return -erf(-x);
	double[] a =
	{0.254829592,-0.284496736,1.421413741,-1.453152027,1.061405429};
	double t=1/(1+0.3275911*x);
	double sum=t*(a[0]+t*(a[1]+t*(a[2]+t*(a[3]+t*a[4]))));
	return 1.0-sum*Exp(-x*x);
}

public static complex erf(complex x){
// single precision error function (Abramowitz and Stegun)
//	if(x<0) return -erf(-x);
	double[] a =
	{0.254829592,-0.284496736,1.421413741,-1.453152027,1.061405429};
	complex t=1/(1+0.3275911*x);
	complex sum=t*(a[0]+t*(a[1]+t*(a[2]+t*(a[3]+t*a[4]))));
	return 1.0-sum*cmath.exp(-x*x);
}
}//math
