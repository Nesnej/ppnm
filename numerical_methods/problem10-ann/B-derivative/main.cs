using System;
using static System.Math;
using static System.Console;
public static class main{
public static void Main(){
	int npoints = 30;
	double a=0, b=2*PI;
	vector xs = new vector(npoints),
	       ys = new vector(npoints);
	for(int i=0;i<npoints;i++){
		xs[i] = a + (b-a)*i/npoints;
		ys[i] = Cos(xs[i]);
	}//for

	Func<double,double> f = (t) => t*Exp(-t*t);
	Func<double,double> df = (t) => (1-2*t*t)*Exp(-t*t);
	Func<double,double> af = (t) => -Exp(-t*t)/2;
	int n = 7;
	var cosnet = new network(n,f,df,af);
	Error.Write("Cosine training log:\n");
	cosnet.train(xs,ys,method:"qnewton",acc:1e-6,log:true);
	
	for(double x=0;x<b;x+=1D/32){
		double y = cosnet.eval(x);
		double z = cosnet.derivative(x);
		double u = cosnet.antiderivative(x,0,0);
		double w = cosnet.antiderivative(x);
		Write($"{x} {y} {z} {u} {w}\n");
	}//for

	Error.Write("\n\n");
	Write("\n\n");

	n = 15;
	for(int i=0;i<npoints;i++){
		ys[i] = Cos(2*xs[i])*Exp(-(xs[i]-PI)*(xs[i]-PI)/2);
	}//for
	var net = new network(n,f,df,af);
	Error.Write("Cosine times gaussian training log:\n");
	net.train(xs,ys,method:"simplex",acc:1e-6,log:true, lim:5000);

	for(double x=0;x<b;x+=1D/32){
		double y = net.eval(x);
		double z = net.derivative(x);
		double u = net.antiderivative(x,PI,0);
		double w = net.antiderivative(x);
		complex z1 = new complex(x-PI,2);
		complex z2 = new complex(PI-x,2);
		complex Z = Sqrt(PI/2)/2/Exp(2)*(math.erf(z1/Sqrt(2)) - math.erf(z2/Sqrt(2)));
		Write($"{x} {y} {z} {u} {w} {Z.Re}\n");
	}//for
}//Main
}//main
