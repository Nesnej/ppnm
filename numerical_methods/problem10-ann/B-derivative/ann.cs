using System;
using System.Diagnostics;
using static System.Double;
using static System.Console;
public class network{

// Defining network parameters
public vector p;
public Func<double,double> f;
public Func<double,double> df;
public Func<double,double> af;
public readonly int n;
public double xmin;

public network(int n, Func<double,double> f,
	// Initialize network parameters
	Func<double,double> df,
	Func<double,double> af){
	this.n = n;
	this.f = f;
	this.df = df;
	this.af = af;
	this.p = new vector(3*n);
}//network

// Method that evaluate the network function in a point x
public double eval(double x){
	double sum=0;
	for(int i=0;i<n;i++){
		double a = p[0+3*i];
		double b = p[1+3*i];
		double w = p[2+3*i];
		sum += f((x-a)/b)*w;
	}//for
	return sum;
}//feed

// Method that evaluate the derivative of the network function in a point x
public double derivative(double x){
	double sum=0;
	for(int i=0;i<n;i++){
		double a = p[0+3*i];
		double b = p[1+3*i];
		double w = p[2+3*i];
		sum += df((x-a)/b)*w/b;
	}//for
	return sum;
}//feed

// Method that evaluate the antiderivative of the network function in a point x
public double antiderivative(double x, double x0=NaN, double f0=NaN){
	double sum=0;
	for(int i=0;i<n;i++){
		double a = p[0+3*i];
		double b = p[1+3*i];
		double w = p[2+3*i];
		sum += af((x-a)/b)*w*b;
		// If no reference point is set subtract the antiderivative
		// evaluated at the smallest training point
		if(IsNaN(x0) && IsNaN(f0)) sum -= af((xmin-a)/b)*w*b;
		// If a reference point is set subtract the difference between the reference point
		// and the antiderivative of each network function to set the integration constant
		else{
			// Terminate if only x0 or f0 is set
			Trace.Assert(IsNaN(x0),"x0 must be a number");
			Trace.Assert(IsNaN(f0),"f0 must be a number");
			sum += f0 - af((x0-a)/b)*w*b;
		}//else
	}//for
	return sum;
}//antiderivative

// Method that optimizes the function parameters p to fit a dataset
public void train(vector x, vector y, string method="qnewton",
		double acc=1e-2, int lim=3000, bool log=false){
	int ncalls=0;
	// Initializing network parameters for optimization and find the smallest training point
	xmin = x[0];
	for(int i=0;i<n;i++){
		// Distribute the network functions evenly across the training interval
		p[0+3*i] = (x[x.size-1]-x[0])*i/(n-1);
		p[1+3*i] = 1D;
		p[2+3*i] = 1D;
		if(xmin>x[i]) xmin = x[i];
	}//for
	// Function that determines the sum square deviances from the tabulated points
	Func<vector,double> delta = (u) => {
		ncalls++;
		p = u;
		double sum=0;
		for(int j=0;j<x.size;j++){
			sum += (eval(x[j])-y[j])*(eval(x[j])-y[j]);
		}//for
		return sum/x.size;
	};//delta
	vector v = p;
	int nsteps=0;
	// Optimize the network parameters with the method of the users choice
	if(method=="qnewton") nsteps = mini.qnewton_sr1(delta, ref v, acc:acc, lim:lim, log:log);
	else if(method=="simplex") nsteps = mini.simplex(delta, ref v, acc:acc, dx:.2, lim:lim);
	// Terminate is unknown method is called for
	else Trace.Assert(true, "Unknown minimization method " + method
			+ "\nChoose either qnewton for quasi newton or simplex for downhill simplex.");
	p = v;
	// If asked for write information of the network to the error stream
	if(log){
		Error.Write("Network log:\n");
		Error.Write($"Number of Function calls       : ncalls = {ncalls}\n");
		Error.Write($"Number of Steps in Optimization: nsteps = {nsteps}\n");
		Error.Write("\n");
	}//if
}//train
}//network
