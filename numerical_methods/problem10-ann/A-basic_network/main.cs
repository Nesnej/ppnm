using System;
using static System.Math;
using static System.Console;
public static class main{
public static void Main(){
	int npoints = 30;
	double a=0, b=2*PI;
	vector xs = new vector(npoints),
	       ys = new vector(npoints);
	for(int i=0;i<npoints;i++){
		xs[i] = a + (b-a)*i/npoints;
		ys[i] = Cos(xs[i]);
	}//for

	Func<double,double> f = (x) => x*Exp(-x*x);
	int n = 5;
	var cosnet = new network(n,f);
	var cosnets = new network(n,f);
	Error.Write("\nCosine test:\n");
	Error.Write("Training with Quasi Newton\n");
	cosnet.train(xs,ys,method:"qnewton",log:true);
	Error.Write("Training with Downhill Simplex\n");
	cosnets.train(xs,ys,method:"simplex",log:true);
	
	int n_test_points = 25;
	var rnd = new Random();
	for(int i=0;i<n_test_points;i++){
		double x = b*rnd.NextDouble();
		double y = cosnet.eval(x);
		double z = cosnets.eval(x);
		Write($"{x} {y} {z}\n");
	}//for

	Error.Write("\nCosine-Gaussian product test:\n");
	Write("\n\n");

	n = 7;
	for(int i=0;i<npoints;i++){
		ys[i] = Cos(2*xs[i])*Exp(-(xs[i]-PI)*(xs[i]-PI)/2);
	}//for
	var net = new network(n,f);
	var nets = new network(n,f);
	Error.Write("Training with Quasi Newton\n");
	net.train(xs,ys,method:"qnewton",log:true);
	Error.Write("Training with Downhill Simplex\n");
	nets.train(xs,ys,method:"qnewton",log:true);

	for(int i=0;i<n_test_points;i++){
		double x = b*rnd.NextDouble();
		double y = net.eval(x);
		double z = nets.eval(x);
		Write($"{x} {y} {z}\n");
	}//for
}//Main
}//main
