using System;
using System.Diagnostics;
using static System.Console;
public class network{

// Defining network parameters
public vector p;
public Func<double,double> f;
public readonly int n;

public network(int n, Func<double,double> f){
	// Initialize network parameters
	this.n = n;
	this.f = f;
	this.p = new vector(3*n);
}//network

// Method that evaluate the network function in a point x
public double eval(double x){
	double sum=0;
	for(int i=0;i<n;i++){
		double a = p[0+3*i];
		double b = p[1+3*i];
		double w = p[2+3*i];
		sum += f((x-a)/b)*w;
	}//for
	return sum;
}//feed

// Method that optimizes the function parameters p to fit a dataset
public void train(vector x, vector y, string method="qnewton",
		double acc=1e-2, int lim=3000, bool log=false){
	int ncalls=0;
	// Initializing network parameters for optimization
	for(int i=0;i<n;i++){
		// Distribute the network functions evenly across the training interval
		p[0+3*i] = (x[x.size-1]-x[0])*i/(n-1);
		p[1+3*i] = 1D;
		p[2+3*i] = 1D;
	}//for
	// Function that determines the sum square deviances from the tabulated points
	Func<vector,double> delta = (u) => {
		ncalls++;
		p = u;
		double sum=0;
		for(int j=0;j<x.size;j++){
			sum += (eval(x[j])-y[j])*(eval(x[j])-y[j]);
		}//for
		return sum/x.size;
	};//delta
	vector v = p;
	int nsteps=0;
	// Optimize the network parameters with the method of the users choice
	if(method=="qnewton") nsteps = mini.qnewton_sr1(delta, ref v, acc:acc, lim:lim);
	else if(method=="simplex") nsteps = mini.simplex(delta, ref v, acc:acc, dx:.2, lim:lim);
	// Terminate is unknown method is called for
	else Trace.Assert(true, "Unknown minimization method " + method
			+ "\nChoose either qnewton for quasi newton or simplex for downhill simplex.");
	p = v;
	// If asked for write information of the network to the error stream
	if(log){
		Error.Write("Network log:\n");
		Error.Write($"Number of Function calls       : ncalls = {ncalls}\n");
		Error.Write($"Number of Steps in Optimization: nsteps = {nsteps}\n");
		Error.Write("\n");
	}//if
}//train
}//network
