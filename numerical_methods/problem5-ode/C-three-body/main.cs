using static System.Console;
using static System.Math;
using System;
using System.Collections;
using System.Collections.Generic;
static class main{
static void Main(){
	double t0 = 0, tend = 15, mg = 1;
	vector r10 = new vector(new double[] {0.97000436, -0.24308753});
	vector r20 = -1*r10.copy();
	vector r30 = new vector(new double[] {0, 0});
	vector v30 = new vector(new double[] {-.93240737, -.86473146});
	vector v20 = -1*v30.copy()/2;
	vector v10 = v20.copy();
	vector y0 = new vector(new double[] {r10[0], r10[1],
			r20[0], r20[1], r30[0], r30[1],
			v10[0], v10[1], v20[0], v20[1],
			v30[0], v30[1]});
	
	List<double> ts = new List<double>();
	List<vector> ys = new List<vector>();
	ode.rk45(eom(mg),t0,y0,tend,h:1e-3,xlist:ts,ylist:ys,acc:1e-5,eps:1e-5);
	for(int i=0;i<ts.Count;i++){
		Write($"{i}");
		for(int j = 0;j<y0.size/2;j++) Write($" {ys[i][j]}");
		Write("\n");
	}//for
} //Main

public static Func<double,vector,vector> eom(double mg){
	return (x,y) => {
		// The coordinates are placed in the vector y with indecies 0-5.
		// Indices 6-11 are used are used to store the coordinate derivatives in the same order
		vector r1 = new vector(new double[] {y[0], y[1]});
		vector r2 = new vector(new double[] {y[2], y[3]});
		vector r3 = new vector(new double[] {y[4], y[5]});
		vector ddr1 = -mg*(r1-r2)/Pow((r1-r2).norm(),3) - mg*(r1-r3)/Pow((r1-r3).norm(),3);
		vector ddr2 = -mg*(r2-r1)/Pow((r1-r2).norm(),3) - mg*(r2-r3)/Pow((r2-r3).norm(),3);
		vector ddr3 = -mg*(r3-r1)/Pow((r1-r3).norm(),3) - mg*(r3-r2)/Pow((r2-r3).norm(),3);
		return new vector(new double[] {y[6], y[7], y[8], y[9], y[10], y[11]
				, ddr1[0], ddr1[1], ddr2[0], ddr2[1], ddr3[0], ddr3[1]});
	};//return
}//eom
} //main
