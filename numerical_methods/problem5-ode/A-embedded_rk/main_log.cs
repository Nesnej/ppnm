using static System.Console;
using static System.Math;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
static class main{
static void Main(){
	double y0 = .5;
	double a = 0;
	double b = 3;
	double acc = 1e-4, eps = 1e-4;
	Func<double,vector,vector> diff_eq=(x,y) => new vector(y[0]*(1-y[0]));
	vector ya = new vector(y0);
	List<double> xs = new List<double>();
	List<vector> ys = new List<vector>();
	ode.rk12(diff_eq,a,ya,b,xlist:xs,ylist:ys,acc:acc,eps:eps);
	int N = xs.Count;
	for(int i=0;i<N;i++)
		WriteLine($"{xs[i]} {ys[i][0]}");
	Write("\n\n");
	
	double eps_x=1.0/32, dx=1.0/16;
	Func<double,double> log=(x) => 1/(1+Exp(-x));
	for(double x=0+eps_x;x<=3;x+=dx)
		WriteLine("{0,10:f3} {1,15:f8}",x,log(x));
	
	double tol_des = acc + log(xs[N-1])*eps;
	double tol_ach = ys[N-1][0] - log(xs[N-1]);
	var out_tol = File.AppendText("out.tol.txt");
	out_tol.Write("\nTolerance test of the RK12 solution of the logistic differential equation\n"
			+$"Desired tolerance:  {tol_des}\n"
			+$"Achieved tolerance: {tol_ach}\n");
	if(tol_des>=tol_ach) out_tol.Write("The desired tolerance has been achieved.\n");
	else out_tol.Write("TEST FAILED! The desired tolerance has not been achieved.\n");
	out_tol.Close();
} //Main
} //main
