using System;
public partial class ode{
static vector[] rkstep12(Func<double,vector,vector> f, double t, vector yt, double h){
	// Derivative vector at the point t
	vector k0 = f(t,yt);
	// Derivative vector at the midpoint
	vector k = f(t+.5*h,yt+.5*h*k0);
	// Midpoint step
	vector yh = yt + h*k;
	// Error as difference in midpoint and Euler step
	vector err = (k-k0)*h;
	vector[] res = {yh,err};
	return res;
}//step
}//class
