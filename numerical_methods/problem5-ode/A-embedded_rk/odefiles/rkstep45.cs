using System;
public partial class ode{
static vector[] rkstep45(Func<double,vector,vector> f, double t, vector yt, double h){
	// Derivative vectors at certain points according to the Runge-Kutta Fehlberg
	// Butcher's tableau on the Wikipedia page
	vector k1 = h*f(t,yt);
	vector k2 = h*f(t + .25*h, yt + .25*k1);
	vector k3 = h*f(t + 3D/8*h, yt + 3D/32*k1 + 9D/32*k2);
	vector k4 = h*f(t + 12D/13*h, yt + 1932D/2197*k1 - 7200D/2197*k2 + 7296D/2197*k3);
	vector k5 = h*f(t + h, yt + 439D/216*k1 - 8*k2 + 3680D/513*k3 - 845D/4104*k4);
	vector k6 = h*f(t + .5*h, yt - 8D/27*k1 + 2*k2 - 3544D/2565*k3 + 1859D/4104*k4 - 11D/40*k5);
	vector k = 16D/135*k1 + 6656D/12825*k3 + 28561D/56430*k4 - 9D/50*k5 + 2D/55*k6;
	// RKF4(5) step
	vector yh = yt + k;
	// RKF4(5) error estimate
	vector err = k - (25D/216*k1 + 1408D/2565*k3 + 2197D/4104*k4 - 1D/5*k5);
	vector[] res = {yh,err};
	return res;
}//step
}//class
