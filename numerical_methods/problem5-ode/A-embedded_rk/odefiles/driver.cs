using System;
using System.Collections.Generic;
using static System.Math;
using static System.Console;
public partial class ode{
public static vector driver(Func<double,vector,vector> F, double a, vector ya, double b,
		double acc, double eps, double h, List<double> xlist, List<vector> ylist,
		int lim, Func<Func<double,vector,vector>, double, vector, double, vector[]> stepper) {
	int nstep = 0;
	// Reserve a for the initial point and use x for the continually updating point
	double x = a;
	vector yx = ya;
	// If lists are not initialized initialize them with the initial point
	if(xlist!=null) {xlist.Clear(); xlist.Add(a);}
	if(ylist!=null) {ylist.Clear(); ylist.Add(ya);}
	// Continue driving until the endpoint is reached
	while(x<b){
		// Terminate if maximum number of steps is exceeded
		if(nstep>lim) {Error.Write($"ode driver: number of steps exceeded the limit, {lim}.\n"); return yx;}
		// Bound the step within the closed interval [a,b]
		if(x+h>b) h=b-x;
		// Try a step of size h
		vector[] trial = stepper(F,x,yx,h);
		vector yh = trial[0]; vector err = trial[1];
		vector tol = new vector(err.size);
		// Calculate tolerance
		for(int i=0;i<tol.size; i++){
			tol[i] = Max(acc,Abs(eps*yh[i]))*Sqrt(h/(b-a));
			if(err[i]==0) err[i] = tol[i]/4;
		}//for
		// Finding the smallest tolerance to error ratio
		double err_fac = Abs(tol[0]/err[0]);
		bool crit = true;
		for(int i=1; i<tol.size; i++){
			err_fac = Min(err_fac,Abs(tol[i]/err[i]));
			// If the estimated error exceeds tolerance the step is bad and the step
			// acceptance criterion is set to false. If the error is too large
			// there is no reason to search the entire error vector and the loop is
			// stoped by setting i=tol.size
			if(Abs(err[i])>tol[i]){
				crit=false;
				i=tol.size;
			}//if
		}//for
		// If the estimated error is smaller than tolerance accept the step
		if(crit){
			x += h; yx = yh; nstep++;
			xlist.Add(x); ylist.Add(yx);
		}//if
		else Error.Write($"driver: bad step at {x}\n");
		// Determine next step size
		h *= Min(Pow(err_fac,.25)*.95,2);
	}//while
	return yx;
}//driver
}//ode
