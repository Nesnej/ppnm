using System;
using System.Collections.Generic;
public partial class ode{
// Function that calls the driver with the RK1(2) stepper
public static vector rk12(Func<double,vector,vector> F, double a, vector ya,
		double b, double acc=1e-3, double eps=1e-3, double h=.1,
		List<double> xlist=null, List<vector> ylist=null, int lim=999){
	return driver(F, a, ya, b, acc, eps, h, xlist, ylist, lim, rkstep12);
}//rk12

// Function that calls the driver with the RK4(5) stepper
public static vector rk45(Func<double,vector,vector> F, double a, vector ya,
		double b, double acc=1e-3, double eps=1e-3, double h=.1,
		List<double> xlist=null, List<vector> ylist=null, int lim=999){
	return driver(F, a, ya, b, acc, eps, h, xlist, ylist, lim, rkstep45);
}//rk12

}//ode
