using static System.Console;
using static System.Math;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
static class main{
static void Main(){
	double ya0 = 0;
	double ya1 = 1;
	double a = 0;
	double b = 3;
	double acc = 1e-4, eps = 1e-4;
	Func<double,vector,vector> diff_eq=(x,y) => new vector(y[1],-y[0]);
	vector ya = new vector(ya0,ya1);
	List<double> xs = new List<double>();
	List<vector> ys = new List<vector>();
	ode.rk45(diff_eq,a,ya,b,xlist:xs,ylist:ys,acc:acc,eps:eps);
	int N = xs.Count;
	for(int i=0;i<N;i++)
		WriteLine($"{xs[i]} {ys[i][0]}");
	Write("\n\n");

	double dx=1.0/16;
	for(double x=0;x<=xs[N-1];x+=dx)
		WriteLine($"{x} {Sin(x)}");
	
	double tol_des = acc + Sin(xs[N-1])*eps;
	double tol_ach = ys[N-1][0] - Sin(xs[N-1]);
	var out_tol = File.AppendText("out.tol.txt");
	out_tol.Write("\nTolerance test of the RK45 solution of the harmonic oscillator differential equation\n"
			+$"Desired tolerance:  {tol_des}\n"
			+$"Achieved tolerance: {tol_ach}\n");
	if(tol_des>=tol_ach) out_tol.Write("The desired tolerance has been achieved.\n");
	else out_tol.Write("TEST FAILED! The desired tolerance has not been achieved.\n");
	out_tol.Close();
} //Main
} //main
