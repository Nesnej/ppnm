using static System.Console;
using static System.Math;
using System;
using System.Collections;
using System.Collections.Generic;
static class main{
static void Main(string[] args){
	double t0 = 0, tc = 10, tr = 2.2*tc;
	double n = 5e6, i0 = 139, s0 = n-i0, r0 = 0;
	double tend = 60;
	foreach(string str in args){
		string[] ws = str.Split('=');
		if(ws[0] == "tr") tr = double.Parse(ws[1]);
		if(ws[0] == "tc") tc = double.Parse(ws[1]);
		if(ws[0] == "tend") tend = double.Parse(ws[1]);
	}//foreach
	Func<double,vector,vector> diff_eq=(x,y) => new vector(-y[0]*y[1]/(n*tc),y[0]*y[1]/(n*tc)-y[1]/tr,y[1]/tr);
	vector ya = new vector(s0,i0,r0);
	List<double> xs = new List<double>();
	List<vector> ys = new List<vector>();
	ode.rk45(diff_eq,t0,ya,tend,xlist:xs,ylist:ys,acc:1e-6,eps:1e-6);
	for(int i=0;i<xs.Count;i++){
		Write($"{xs[i]}");
		for(int j = 0;j<ya.size;j++) Write($" {ys[i][j]}");
		Write("\n");
	}//for
	Write("\n\n");
} //Main
} //main
