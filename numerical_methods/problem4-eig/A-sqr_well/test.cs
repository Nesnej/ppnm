using static System.Console;
using static System.Math;
public class main{
public static void Main(){
	int n=20;
	double s=1.0/(n+1);
	matrix H = new matrix(n,n);
	for(int i=0;i<n-1;i++){
		matrix.set(H,i,i,-2);
		matrix.set(H,i,i+1,1);
		matrix.set(H,i+1,i,1);
	}//for
	matrix.set(H,n-1,n-1,-2);
	matrix.scale(H,-1/s/s);
	matrix V = new matrix(n,n);
	vector e = new vector(n);
	matrix A = H.copy();
	int sweeps = jacobi.cyclic(A,e,V);
	Write("Test of Eigenvalue Decomposition on the Quantum Mechanical Square Well\n");
	Write($"sweeps: {sweeps}\n");
	int score = 0;
	int N = n/3;
	Write($"Number of Eigenvalues: {N}\n");
	WriteLine("k	Numeric Eigenvalue	Exact Eigenvalue");
	for (int k=0; k < N; k++){
		double exact = PI*PI*(k+1)*(k+1);
		WriteLine("{0,1:g3} {1,10:g3}		  {2,10:g3}",k,e[k],exact);
		if(cmath.approx(e[k],exact,acc:1e-2,eps:1e-1)) score+=1;
	}
	if(score == N) Write($"All eigenvalues match to acc=1e-2 and eps=1e-1 with n={n}.\n");
	else Write($"Not all eigenvalues match!\n");
}//Main
}//main
