using System;
using static System.Math;
public class jacobi{
public static int cyclic(matrix A, vector e, matrix V){
	bool changed=true; int sweeps=0, n=A.size1;
	double app,apq,aqq,phi,c,s,app1,aqq1,aip,aiq,vip,viq;
	int p,q;
	// Setting up e and V
	for(int i=0;i<n;i++){
		e[i] = A[i,i];
		V[i,i] = 1D;
		for(int j=i+1;j<n;j++){
			V[i,j] = 0D;
			V[j,i] = V[i,j];
		}//for
	}//for
	while(changed){
		sweeps++; changed=false;
		for(q=n-1;q>0;q--){
		// Performing sweep
		for(p=0;p<q;p++){
			// Setting the matrix element p,q to 0 by Givens rotation
			app = e[p] ; aqq = e[q]; apq = A[p,q];
			phi = Atan2(2*apq,aqq-app)/2;
			c = Cos(phi); s = Sin(phi);
			app1 = c*c*app - 2*s*c*apq + s*s*aqq;
			aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
			// Applying rotation
			if(app1!=app || aqq1!=aqq){
				changed = true; e[q] = aqq1; e[p] = app1; A[p,q] = 0D;
				for(int i=0;i<p;i++){
					aip = A[i,p]; aiq = A[i,q];
					A[i,p] = c*aip - s*aiq;
					A[i,q] = s*aip + c*aiq;
				}//for
				for(int i=p+1;i<q;i++){
					aip = A[p,i]; aiq = A[i,q];
					A[p,i] = c*aip - s*aiq;
					A[i,q] = s*aip + c*aiq;
				}//for
				for(int i=q+1;i<n;i++){
					aip = A[p,i]; aiq = A[q,i];
					A[p,i] = c*aip - s*aiq;
					A[q,i] = s*aip + c*aiq;
				}//for
				// Updating the eigenvectors
				if(V!=null)for(int i=0;i<n;i++){
					vip = V[i,p]; viq = V[i,q];
					V[i,p] = c*vip - s*viq;
					V[i,q] = s*vip + c*viq;
				}//for
			}//if
		}//for
		}//for
	}//while
	return sweeps;
}//cyclic
}//jacobi
