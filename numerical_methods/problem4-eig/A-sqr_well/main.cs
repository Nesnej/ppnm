using static System.Console;
using static System.Math;
public class main{
public static void Main(string[] args){
	int n=40, Npsi = 3, Neig = 20;
	foreach(string str in args){
		string[] ws = str.Split('=');
		if(ws[0] == "n") n = int.Parse(ws[1]);
		if(ws[0] == "Npsi") Npsi = int.Parse(ws[1]);
		if(ws[0] == "Neig") Neig = int.Parse(ws[1]);
	}//foreach
	double s=1.0/(n+1);
	matrix H = new matrix(n,n);
	for(int i=0;i<n-1;i++){
		matrix.set(H,i,i,-2);
		matrix.set(H,i,i+1,1);
		matrix.set(H,i+1,i,1);
	}//for
	matrix.set(H,n-1,n-1,-2);
	matrix.scale(H,-1/s/s);
	matrix V = new matrix(n,n);
	vector e = new vector(n);
	matrix A = H.copy();
	jacobi.cyclic(A,e,V);
	double a = .35;
	for(int k=0;k<Npsi;k++){
		int y = (n-1)/(2+2*k);
		double norm = a/Abs(V[y,k])*Sign(V[y,k]*Sin((y+1D)/(n-1)));
		WriteLine($"{0} {0+k} {0+k}");
		for(int i=0;i<n;i++) {
			double x = (i+1.0)/(n+1);
			double arg = (k+1)*x*PI;
			WriteLine($"{x} {norm*V[i,k]+k} {a*Sin(arg)+k}");
		}//for
		WriteLine($"{1} {0+k} {0+k}");
		Write("\n\n");
	}//for
	for(int k=0; k <= Neig; k++){
		WriteLine($"{k} {e[k]} {e[k]*5e-2}");
	}//for
	Write("\n\n");
	for(double k=0; k < Neig; k+=1D/16){
		double exact = PI*PI*(k+1)*(k+1);
		WriteLine($"{k} {exact}");
	}//for
}//Main
}//main
