using System;
using System.Diagnostics;
using static System.Math;
using static System.Console;
public class main{
public static void Main(){
	int n0=2, nmax = 50;
	// It apparently takes longer to run a method in a class the first times,
	// thus a dummy run on an identity matrix is done to avoid this problem.
	matrix Q = new matrix(n0,n0); Q.setid();
	vector eig = new vector(n0);
	matrix vec = new matrix(n0,n0);
	jacobi.cyclic(Q,eig,vec);
	jacobi.classic(Q,eig,vec);
	var watch=new Stopwatch();
	watch.Reset();
	watch.Start();
	watch.Stop();
	for(int n = n0;n<=nmax;n++){
		var rnd = new Random();
		matrix A = new matrix(n,n);
		vector e = new vector(n);
		matrix V = new matrix(n,n);
		for(int i=0;i<n;i++){
			A[i,i] = 2*(rnd.NextDouble() - .5);
			for(int j=i+1;j<n;j++){
				A[i,j] = 3*(rnd.NextDouble() - .5);
				A[j,i] = A[i,j];
			}//for
		}//for
		matrix B = A.copy();
		watch.Reset();
		watch.Start();
		int N = jacobi.cyclic(A,e,V);
		watch.Stop();
		Write($"{n} {N*n} {watch.ElapsedTicks} ");
		watch.Reset();
		watch.Start();
		N = jacobi.classic(B,e,V);
		watch.Stop();
		Write($"{N} {watch.ElapsedTicks}\n");
	}//for
}//Main
}//main
