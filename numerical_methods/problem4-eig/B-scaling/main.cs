using System;
using System.Diagnostics;
using static System.Math;
using static System.Console;
public class main{
public static void Main(){
	int n0=1, nmax = 50;
	var watch=new Stopwatch();
	for(int n = n0;n<=nmax;n++){
		var rnd = new Random();
		matrix A = new matrix(n,n);
		vector e = new vector(n);
		matrix V = new matrix(n,n);
		for(int i=0;i<n;i++){
			A[i,i] = 2*(rnd.NextDouble() - .5);
			for(int j=i+1;j<n;j++){
				A[i,j] = 3*(rnd.NextDouble() - .5);
				A[j,i] = A[i,j];
			}//for
		}//for
		watch.Reset();
		watch.Start();
		jacobi.cyclic(A,e,V);
		watch.Stop();
		if(n!=n0) Write($"{n} {watch.ElapsedTicks}\n");
	}//for
}//Main
}//main
