using System;
using static System.Console;
public class main{
public static void Main(){
	int n = 5;
	var rnd = new Random(1);
	matrix A = new matrix(n,n);
	vector e = new vector(n);
	matrix V = new matrix(n,n);
	for(int i=0;i<n;i++){
		A[i,i] = 2*(rnd.NextDouble() - .5);
		for(int j=i+1;j<n;j++){
			A[i,j] = 3*(rnd.NextDouble() - .5);
			A[j,i] = A[i,j];
		}//for
	}//for
	Write("Eigenvalue Decomposition Test\n");
	A.print("Random Symmetric Matrix: \n");
	matrix B = A.copy();
	int sweeps = jacobi.classic(A,e,V);
	A.print("Reduced Matrix: \n");
	Write($"Number of Sweeps {sweeps}\n");
	e.print("Eigenvalues: ");
	V.print("Eigenvectors: ");
	Write("The Eigenvalue-decomposed Matrix should be diagonal.\n");
	(V.T*B*V).print("Eigenvalue-decomposed Matrix Full Precision: ");
	(V.T*B*V).print("Eigenvalue-decomposed Matrix Rounded: ", "{0,9:F3}");
}//Main
}//main
