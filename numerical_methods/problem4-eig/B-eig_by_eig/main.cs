using System;
using static System.Console;
public class main{
public static void Main(){
	int n = 5;
	var rnd = new Random(1);
	matrix A = new matrix(n,n);
	vector e = new vector(n);
	matrix V = new matrix(n,n);
	for(int i=0;i<n;i++){
		A[i,i] = 2*(rnd.NextDouble() - .5);
		for(int j=i+1;j<n;j++){
			A[i,j] = 3*(rnd.NextDouble() - .5);
			A[j,i] = A[i,j];
		}//for
	}//for
	matrix B = A.copy();
	vector E = e.copy();
	int sweeps = jacobi.cyclic(B,E,V);
	Write("Eigenvalue Decomposition Test\n");
	A.print("Random Symmetric Matrix: \n");
	Write("\n");
	for(int k=1;k<=n;k++){
		B = A.copy();
		Write($"Calculating the first {k} eigenvalue(s)\n");
		sweeps = jacobi.cyclic(A,e,V,k,true);
		Write($"Number of Sweeps: {sweeps}\n");
		e.print("Eigenvalues                     : ");
		E.print("Full Diagonalization Eigenvalues: ");
		Write("\n");
		A = B.copy();
	}//for
	for(int k=n;k>0;k--){
		B = A.copy();
		Write($"Calculating the last {k} eigenvalue(s)\n");
		sweeps = jacobi.cyclic(A,e,V,k,false);
		Write($"Number of Sweeps: {sweeps}\n");
		e.print("Eigenvalues                     : ");
		Write("Full Diagonalization Eigenvalues: ");
		for(int i=E.size-1;i>=0;i--) System.Console.Write("{0,10:g3} ",E[i]);
		System.Console.Write("\n");
		Write("\n");
		A = B.copy();
	}//for
}//Main
}//main
