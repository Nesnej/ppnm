using System;
using System.Diagnostics;
using static System.Math;
public partial class jacobi{
public static int cyclic(matrix A, vector e, matrix V){
	bool changed=true; int sweeps=0, n=A.size1;
	// Setting up e and V
	for(int i=0;i<n;i++){
		e[i] = A[i,i];
		V[i,i] = 1D;
		for(int j=i+1;j<n;j++){
			V[i,j] = 0D;
			V[j,i] = V[i,j];
		}//for
	}//for
	while(changed){
		// Performing sweep
		changed=false; sweeps++;
		for(int q=n-1;q>0;q--){
			for(int p=0;p<q;p++){
				// Setting the matrix element p,q to 0 by Givens rotation
				changed = rotation(A,V,e,p,q);
			}//for
		}//for
	}//while
	return sweeps;
}//cyclic

public static int cyclic(matrix A, vector e, matrix V,int neig,bool low=true){
	int sweeps=0, n=A.size1;
	// Setting up e and V
	for(int i=0;i<n;i++){
		e[i] = A[i,i];
		V[i,i] = 1D;
		for(int j=i+1;j<n;j++){
			V[i,j] = 0D;
			V[j,i] = V[i,j];
		}//for
	}//for
	for(int p=0;p<neig;p++){
		// Performing sweep
		bool changed=true; sweeps++;
		while(changed){
			changed=false;
			for(int q=p+1;q<n;q++){
				// Setting the matrix element p,q to 0 by Givens rotation
				changed = rotation(A,V,e,p,q,low);
			}//for
		}//while
	}//for
	return sweeps;
}//cyclic

public static bool rotation(matrix A, matrix V, vector e, int p, int q,bool low=true){
	double app = e[p], aqq = e[q], apq = A[p,q];
	double phi;
	if(low) phi = Atan2(2*apq,aqq-app)/2;
	else phi = Atan2(-2*apq,-aqq+app)/2;
	double c = Cos(phi), s = Sin(phi);
	double app1 = c*c*app - 2*s*c*apq + s*s*aqq;
	double aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
	int n=A.size1;
	// Applying rotation
	if(app1!=app || aqq1!=aqq){
		e[q] = aqq1; e[p] = app1; A[p,q] = 0D;
		for(int i=0;i<p;i++){
			double aip = A[i,p], aiq = A[i,q];
			A[i,p] = c*aip - s*aiq;
			A[i,q] = s*aip + c*aiq;
		}//for
		for(int i=p+1;i<q;i++){
			double aip = A[p,i], aiq = A[i,q];
			A[p,i] = c*aip - s*aiq;
			A[i,q] = s*aip + c*aiq;
		}//for
			for(int i=q+1;i<n;i++){
			double aip = A[p,i], aiq = A[q,i];
			A[p,i] = c*aip - s*aiq;
			A[q,i] = s*aip + c*aiq;
		}//for
		// Updating the eigenvectors
		if(V!=null)for(int i=0;i<n;i++){
			double vip = V[i,p], viq = V[i,q];
			V[i,p] = c*vip - s*viq;
			V[i,q] = s*vip + c*viq;
		}//for
		return true;
	}//if
	else return false;
}//sweep
}//jacobi
