using System;
using static System.Console;
class main{
static void Main(){
	int n=4;
	matrix A = new matrix(n,n);
	vector b = new vector(n);
	var rnd = new Random(1);
	for(int i=0;i<n;i++){
		b[i] = 3*(rnd.NextDouble() - .5);
		for(int j=0;j<n;j++){
			A[i,j] = 3*(rnd.NextDouble() - .5);
		}//for
	}//for
	matrix I = new matrix(n,n);
	I.set_identity();
	Write("Matrix inverse for matrix A\n");
	A.print("matrix A: ");
	var qra = new gramschmidt(A);
	var B = qra.inv();
	B.print("inverse matrix B: ");
	(A*B).print("AB :");
	(I-A*B).print("I-AB :");
	if(I.approx(A*B)) Write("AB=I: test passed\n");
	else Write("AB!=I: test failed\n");
}//Main
}//main
