using System;
using static System.Console;
class main{
static void Main(){
	int n=4;
	matrix A = new matrix(n,n);
	vector b = new vector(n);
	var rnd = new Random(1);
	for(int i=0;i<n;i++){
		b[i] = 3*(rnd.NextDouble() - .5);
		for(int j=0;j<n;j++){
			A[i,j] = 3*(rnd.NextDouble() - .5);
		}//for
	}//for
	Write("Solution of the linear system of equations Ax=b\n");
	A.print("matrix A: ");
	b.print("vector b: ");
	var qra = new givens(A);
	var x = qra.solve(b);
	x.print("solution vector x: ");
	(A*x).print("Ax :");
	(A*x-b).print("Ax-b :");
	if(b.approx(A*x)) Write("Ax=b: test passed\n");
	else Write("Ax!=b: test failed\n");
}//Main
}//main
