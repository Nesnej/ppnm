using static System.Math;
public class givens{
public readonly matrix R;
public readonly int n,m;
public givens(matrix A){
	n=A.size1; m=A.size2;
	R = A.copy();
	for(int q=0; q<m; q++){
		for(int p=q+1; p<n; p++){
			double phi = Atan2(R[p,q],R[q,q]);
			for(int k=q;k<m;k++){
				double xp = R[p,k];
				double xq = R[q,k];
				R[p,k] = xp*Cos(phi) - xq*Sin(phi);
				R[q,k] = xq*Cos(phi) + xp*Sin(phi);
			}//for
			R[p,q] = phi;
		}//for
	}//for
}//constructor

public vector solve(vector b){
	vector x = b.copy();
	for(int q=0;q<m;q++){
		for(int p=q+1;p<n;p++){
			double phi = R[p,q];
			double xp = x[p];
			double xq = x[q];
			x[p] = xp*Cos(phi) - xq*Sin(phi);
			x[q] = xq*Cos(phi) + xp*Sin(phi);
		}//for
	}//for
	for(int i=x.size-1;i>=0;i--){
		double s = 0;
		for(int k=i+1;k<x.size;k++) s += R[i,k]*x[k];
		x[i] = (x[i] - s)/R[i,i];
	}//for
	return x;
}//solve
}//givens
