using System;
using static System.Console;
class main{
static void Main(){
	int n=4,m=3;
	matrix A = new matrix(n,m);
	var rnd = new Random(1);
	for(int i=0;i<n;i++){
		for(int j=0;j<m;j++){
			A[i,j] = 3*(rnd.NextDouble() - .5);
		}//for
	}//for
	matrix I = new matrix(m,m);
	I.set_identity();
	int score = 0;
	Write("QR-decomposition of random matrix A:\n");
	var qr = new gramschmidt(A);
	var Q = qr.Q; var R = qr.R;
	matrix T = R.copy();
	for(int i=0;i<m;i++){
		for(int j=0;j<i;j++){
			T[i,j] = 0;
		}//for
	}//for
	A.print("matrix A: ");
	R.print("matrix R: ");
	if(R.approx(T)){
		Write("R=T: test passed\n");
		score += 1;
	}//if
	else Write("Q^T*Q!=I: test failed\n");
	Q.print("matrix Q: ");
	(Q.T*Q).print("Q^T*Q: ");
	if((Q.T*Q).approx(I)){
		Write("Q^T*Q=I: test passed\n");
		score += 1;
	}//if
	else Write("Q^T*Q!=I: test failed\n");
	(Q*R).print("matrix Q*R: ");
	(A-Q*R).print("matrix A-QR: ");
	if(A.approx(Q*R)){
		Write("QR=A: test passed\n");
		score += 1;
	}//if
	else Write("QR!=A: test failed\n");
	if(score == 3) Write("ALL TESTS PASSED!\n");
	else Write("NOT ALL TESTS PASSED!\n");
}//Main
}//main
