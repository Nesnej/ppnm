public class gramschmidt{
public readonly matrix Q,R;
public gramschmidt(matrix A){
	Q = A.copy();
	int m = Q.size2;
	R = new matrix(m,m);
	for(int i=0;i<m;i++){
		R[i,i] = Q[i].norm();
		Q[i] /= R[i,i];
		for(int j=i+1;j<m;j++){
			R[i,j] = Q[i]%Q[j];
			Q[j] -= Q[i]*R[i,j];
		}//for
	}//for
}//constructor

public vector solve(vector b){
	vector x = Q.T*b;
	int m = x.size-1;
	double s;
	x[m] /= R[m,m];
	for(int i=m-1;i>=0;i--){
		s = 0;
		for(int k=i+1;k<x.size;k++) s += R[i,k]*x[k];
		x[i] = (x[i]-s)/R[i,i];
	}//for
	return x;
}//solve
}//gramschmidt
