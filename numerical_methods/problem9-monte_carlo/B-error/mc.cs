using System;
using System.Diagnostics;
using static System.Math;
using static System.Console;
public static class mc{
	
public static Random rnd = new Random();

public static vector newx(vector a, vector dx){
	vector x = a.copy();
	for(int i=0;i<x.size;i++) x[i] += rnd.NextDouble()*dx[i];
	return x;
}//newx

public static double[] plain(Func<vector,double> f, vector a, vector b, int N){
	vector dx = b-a;
//	Error.Write("1\n");
	int n = a.size;
	Trace.Assert(n==b.size,"The dimension of a and b are different");
	double vol = 1;
	for(int i=0;i<n;i++) vol *= dx[i];
	double sum=0,sum2=0;
	for(int i=0;i<N;i++){
		double fx = f(newx(a,dx));
		sum += fx;
		sum2 += fx*fx;
	}//for
	double mean = sum/N;
	double sig = Sqrt(sum2/N-mean*mean);
	double sigma = sig/Sqrt(N);
	return new double[] {mean*vol, sigma*vol};
}//plain
}//mc
