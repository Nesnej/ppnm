using System;
using static System.Math;
using static System.Console;
public class main{
public static void Main(string[] args){
	int N = 50000,dn=10;
	foreach(string str in args){
		Error.WriteLine(str);
		string[] ws = str.Split('=');
		if(ws[0] == "N") N = int.Parse(ws[1]);
		if(ws[0] == "dn") dn = int.Parse(ws[1]);
	}//foreach
	Func<vector,double> ho=(x) => x%x;
	vector aho = new vector(0D,0D,0D);
	vector bho = new vector(3D,3D,3D);
	double exact_ho = 243D;
	double[] integral = mc.plain(ho,aho,bho,N);
	double scale = Sqrt(N)*integral[1];
	Write($"{N} {scale/Sqrt(N)} {integral[1]} {Abs(exact_ho-integral[0])}\n");
	for(int n=N-dn;n>0;n-=dn) error(ho,aho,bho,exact_ho,n,scale);
}//Main

public static void error(Func<vector,double> f, vector a, vector b,
		double exact, int N, double scale){
	double[] integral = mc.plain(f,a,b,N);
	Write($"{N} {scale/Sqrt(N)} {integral[1]} {Abs(exact-integral[0])}\n");
}//error
}//main
