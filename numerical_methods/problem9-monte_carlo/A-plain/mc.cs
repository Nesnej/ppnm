using System;
using System.Diagnostics;
using static System.Math;
using static System.Console;
public static class mc{

// Random number generator for the method
public static Random rnd = new Random();

// Function for calculating a new point
public static vector newx(vector a, vector dx){
	vector x = a.copy();
	for(int i=0;i<x.size;i++) x[i] += rnd.NextDouble()*dx[i];
	return x;
}//newx

public static double[] plain(Func<vector,double> f, vector a, vector b, int N){
	// Storing the vector between the integration limits
	vector dx = b-a;
	int n = a.size;
	// Check that the size of the limit vectors are equal
	Trace.Assert(n==b.size,"The dimension of a and b are different");
	// Calculate the volume of integration
	double vol = 1;
	for(int i=0;i<n;i++) vol *= dx[i];
	// Calculate the sum of function values and their squares
	double sum=0,sum2=0;
	for(int i=0;i<N;i++){
		double fx = f(newx(a,dx));
		sum += fx;
		sum2 += fx*fx;
	}//for
	// Calculate mean function value and standard deviation
	double mean = sum/N;
	double sigma = Sqrt((sum2/N-mean*mean)/N);
	// Return integral and error estimate
	return new double[] {mean*vol, sigma*vol};
}//plain
}//mc
