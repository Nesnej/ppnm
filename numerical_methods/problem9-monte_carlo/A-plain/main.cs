using System;
using static System.Math;
using static System.Console;
public class main{
public static void Main(){
	Func<vector,double> sin=(x) => Sin(x[0]);
	vector a = new vector(0D);
	vector b = new vector(PI);
	int N = 2000,n=0;
	double[] integral = mc.plain(sin,a,b,N);
	double exact = 2D;
	Write($"Integral of sine from {a[0]} to {b[0]}\n");
	n += tester(integral,exact,N);
	
	Write("\n");
	
	Func<vector,double> ho=(x) => x%x;
	vector aho = new vector(0D,0D,0D);
	vector bho = new vector(3D,3D,3D);
	N = 15000;
	double[] integral_ho = mc.plain(ho,aho,bho,N);
	double exact_ho = 243D;
	Write($"Integral of 3D isotropic harmonic oscillator potential\n");
	aho.print("Lower Integration Limit:");
	bho.print("Upper Integration Limit:");
	n += tester(integral_ho,exact_ho,N);
	
	Write("\n");
	
	Func<vector,double> complicated=(x) => 1D/(1-Cos(x[0])*Cos(x[1])*Cos(x[2]))*1/(PI*PI*PI);
	vector ac = new vector(0D,0D,0D);
	vector bc = new vector(PI,PI,PI);
	N = 25000;
	double[] integral_c = mc.plain(complicated,ac,bc,N);
	double exact_c = Pow(math.gamma(.25),4)/(4*PI*PI*PI);
	Write($"Integral of [1-cos(x)cos(y)cos(z)]^(-1)\n");
	ac.print("Lower Integration Limit:");
	bc.print("Upper Integration Limit:");
	n += tester(integral_c,exact_c,N);

	Write("\n");
	if(n==0) Write("ALL TESTS PASSED!\n");
	else Write("NOT ALL TESTS PASSED!\n");
	Write("As Monte Carlo integration is based on random points there is an element of\n");
	Write("randomness in whether the tests will pass. The error estimate is quite small\n");
	Write("so one should expect the actual error to exceed the estimate, which the programme\n");
	Write("will classify as a failed test. The test should be considered passed if the estimate is\n");
	Write("resonable, which is to be done manually\n");
}//Main

public static int tester(double[] integral, double exact, int N){
	double dev = Abs(exact - integral[0]);
	Write($"N                : {N}\n");
	Write($"Numeric Integral : {integral[0]}\n");
	Write($"Estimated Error  : {integral[1]}\n");
	Write($"Analytic Integral: {exact}\n");
	Write($"Actual Error     : {dev}\n");
	if(Abs(dev)<integral[1]) {Write("TEST PASSED!\n"); return 0;}
	else Write("TEST FAILED!\n");
	return 1;
}//tester
}//main
