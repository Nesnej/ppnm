using System;
using System.IO;
using static System.Math;
using static System.Console;
public class main{
public static void Main(){
	var outplain = new StreamWriter("sample_plain.txt");
	outplain.Close();
	var outstrat = new StreamWriter("sample_strat.txt");
	outstrat.Close();
	int N = 5000;
	Func<vector,double> ho=(x) => x%x;
	vector aho = new vector(0D,0D);
	vector bho = new vector(3D,3D);
	double exact_ho = 54D;
	double[] ho_p = mc.plain(ho,aho,bho,N,print:true);
	double acc=Abs(exact_ho-ho_p[0]);
	double[] ho_rss = mc.rss(ho,aho,bho,N:10,Nmax:N,acc:acc,print:true);
	var outfile = new StreamWriter("sample_strat.txt",true);
	outfile.Write("\n\n");
	outfile.Close();
	double[] ho_nl = mc.rss(ho,aho,bho,N:10,Nmax:N,acc:0,print:true);
	Write($"Integral of 2D isotropic harmonic oscillator potential\n");
	aho.print("Lower Integration Limit:");
	bho.print("Upper Integration Limit:");
	Write("\n");
	tester(ho_p,ho_rss,ho_nl,exact_ho,N,acc);
//	
//	Write("\n");
//	
//	Func<vector,double> complicated=(x) => 1D/(1-Cos(x[0])*Cos(x[1])*Cos(x[2]))*1/(PI*PI*PI);
//	vector ac = new vector(0D,0D,0D);
//	vector bc = new vector(PI,PI,PI);
//	N = 25000;
//	double[] integral_c = mc.plain(complicated,ac,bc,N);
//	double exact_c = Pow(math.gamma(.25),4)/(4*PI*PI*PI);
//	Write($"Integral of [1-cos(x)cos(y)cos(z)]^(-1)\n");
//	ac.print("Lower Integration Limit:");
//	bc.print("Upper Integration Limit:");
//	tester(integral_c,exact_c,N);
}//Main

public static void tester(double[] integral_p, double[] integral_rss,
		double[] integral_nl, double exact, int N, double acc){
	double dev_p = Abs(exact - integral_p[0]);
	double dev_rss = Abs(exact - integral_rss[0]);
	double dev_nl = Abs(exact - integral_nl[0]);
	Write("Plain Monte Carlo\n");
	Write($"Numeric Integral : {integral_p[0]}\n");
	Write($"Analytic Integral: {exact}\n");
	Write($"Estimated Error  : {integral_p[1]}\n");
	Write($"Actual Error     : {dev_p}\n");
	Write($"N                : {N}\n");
	Write("\n");
	Write("Monte Carlo with Recursive Stratified Sampling\n");
	Write("Desired Accuracy Set to Actual Error of Plain Monte Carlo\n");
	Write($"Numeric Integral : {integral_rss[0]}\n");
	Write($"Analytic Integral: {exact}\n");
	Write($"Estimated Error  : {integral_rss[1]}\n");
	Write($"Actual Error     : {dev_rss}\n");
	Write($"Desired Accuracy : {acc}\n");
	Write($"N                : {integral_rss[2]}\n");
	Write("\n");
	Write("Monte Carlo with Recursive Stratified Sampling\n");
	Write("Desired Accuracy Set to 0\n");
	Write($"Numeric Integral : {integral_nl[0]}\n");
	Write($"Analytic Integral: {exact}\n");
	Write($"Estimated Error  : {integral_nl[1]}\n");
	Write($"Actual Error     : {dev_nl}\n");
	Write($"Desired Accuracy : {0}\n");
	Write($"N                : {integral_nl[2]}\n");
}//tester
}//main
