using System;
using static System.Console;
using static System.Math;
using static System.Double;
public class main{
public static void Main(){
	double inf=PositiveInfinity,ninf=NegativeInfinity;
	int count=0,N=0;
	Func<double,double> gauss=(x) => {count++; return Exp(-x*x);};
	double a=0D,b=inf,exact=Sqrt(PI)/2;
	double acc=1e-9,eps=1e-9;
	double[] q = quad.o4av(gauss,a,b,acc:acc,eps:eps);
	int count4=count; count=0;
	double Q = quad.o8av(gauss,a,b,acc:acc,eps:eps);
	int count8=count;
	Write($"Integral from {a} to {b} of exp(-x^2):\n");
	N += tester(q,Q,a,b,exact,acc,eps,count4,count8);

	Write("\n");

	a=ninf; b=0; count=0;
	q = quad.o4av(gauss,a,b,acc:acc,eps:eps);
	count4=count; count=0;
	Q = quad.o8av(gauss,a,b,acc:acc,eps:eps);
	count8=count;
	Write($"Integral from {a} to {b} of exp(-x^2):\n");
	N += tester(q,Q,a,b,exact,acc,eps,count4,count8);

	Write("\n");

	b=inf; exact*=2; count=0;
	q = quad.o4av(gauss,a,b,acc:acc,eps:eps);
	count4=count; count=0;
	Q = quad.o8av(gauss,a,b,acc:acc,eps:eps);
	count8=count;
	Write($"Integral from {a} to {b} of exp(-x^2):\n");
	N += tester(q,Q,a,b,exact,acc,eps,count4,count8);

	Write("\n");

	Func<double,double> int1=(x) => {count++; return 1D/(x*x+1);};
	a=0;b=inf; exact=PI/2; count=0;
	q = quad.o4av(int1,a,b,acc:acc,eps:eps);
	count4=count; count=0;
	Q = quad.o8av(int1,a,b,acc:acc,eps:eps);
	count8=count;
	Write($"Integral from {a} to {b} of 1/(x^2+1^2):\n");
	N += tester(q,Q,a,b,exact,acc,eps,count4,count8);

	Write("\n");

	Func<double,double> int2=(x) => {count++; return Log((Exp(x)+1)/(Exp(x)-1));};
	exact*=exact; count=0;
//	acc=1e-6; eps=1e-6;
	q = quad.o4av(int2,a,b,acc:acc,eps:eps);
	count4=count; count=0;
	Q = NaN;//quad.o8av(int2,a,b,acc:acc,eps:eps);
	count8=count;
	Write($"Integral from {a} to {b} of ln[(e^x+1)/(e^x-1)]:\n");
	N += tester(q,Q,a,b,exact,acc,eps,count4,count8);
	Write("The method o8av fails this integral, thus the result and #evaluations\n"
		       +"is manually set to NaN and 0 respectively and should thus be disregarded\n\n");
	if(N==0) Write("ALL TESTS PASSED!\n");
	else Write("SOME TEST FAILED!\n");
}//Main

public static int tester(double[] q, double Q,	double a, double b,
		double exact, double acc, double eps, int count4, int count8){
	double tol=acc+eps*exact;
	double err = Abs(exact-q[0]);
	double err8 = Abs(exact-Q);
	Write($"Numerical Integral (o4av): {q[0]}\n");
	Write($"Numerical Integral (o8av): {q[0]}\n");
	Write($"Analytical Integral      : {exact}\n");
	Write($"Tolerance Goal           : {tol}\n");
	Write($"Estimated Error (o4av)   : {q[1]}\n");
	Write($"Actual Error    (o4av)   : {err}\n");
	Write($"Actual Error    (o8av)   : {err8}\n");
	Write($"#Evaluations (o4av)      : {count4}\n");
	Write($"#Evaluations (o8av)      : {count8}\n");
	double sus = 1e-1;
	if(err<=tol){
		if(q[1]<err*sus) Write($"Test suspecious: estimated error less than {sus*1e2}% of actual\n");
		else {Write("TEST PASSED!\n"); return 0;}
	}//if
	else if(err<q[1]) Write("Test suspecious: actual error exceeds estimate.\n");
	else Write("TEST FAILED! Error exceeds tolerance.\n");
	return 1;
}//tester
}//main
