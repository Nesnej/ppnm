using System;
using static System.Math;
using static System.Double;
public static partial class quad{
public static double[] o4acc(Func<double,double> f,
		double a, double b, int lim=99,
		double acc=1e-6, double eps=1e-6){
	// Applying Clenshaw-Curtis variable transformation
	Func<double,double> F=(x) => f((a+b + (b-a)*Cos(x))/2)*Sin(x)*(b-a)/2;
	// Calculate the transformed integral with ordinaty o4a
	return o4a(F,0,PI,lim,acc,eps);
}//o4acc
}//quad
