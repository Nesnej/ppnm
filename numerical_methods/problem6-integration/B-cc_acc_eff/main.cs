using System;
using System.Collections.Generic;
using static System.Console;
using static System.Math;
using static System.Double;
public class main{
public static void Main(){
	int count=0;
	double a=0D,b=1D;
	double eps=0;
	Func<double,double> sqrt_pol=(x) => {count++; return 4*Sqrt(1-x*x);};
	double exact = PI;
	//Write($"Integral from {a} to {b} of 4*sqrt(1-x^2):\n");
	for(double acc=1e-2;acc>1e-12;acc/=1.2){
	double[] q = quad.o4a(sqrt_pol,a,b,acc:acc,eps:eps);
	int count_o4a = count; count=0;
	double[] Q = quad.o4acc(sqrt_pol,a,b,acc:acc,eps:eps);
	int count_o4acc = count; count=0;
	double q8 = quad.o8av(sqrt_pol,a,b,acc:acc,eps:eps);
	int count_o8av = count;
	double tol = acc+eps*exact;
	double err = Abs(exact-q[0]);
	double err_cc = Abs(exact-Q[0]);
	double err_q8 = Abs(exact-q8);
	Write($"{acc} {tol} {err} {err_cc} {err_q8} {count_o4a} {count_o4acc} {count_o8av}\n");
	}//for
//	tester(q,Q,q8,exact,acc,eps,count_o4a,count_o4acc,count_o8av);
}//Main

public static void tester(double[] q, double[] Q, double q8,
		double exact, double acc, double eps,
		int count_o4a, int count_o4acc, int count_o8av){
	double tol = acc+eps*exact;
	double err = Abs(exact-q[0]);
	double err_cc = Abs(exact-Q[0]);
	double err_q8 = Abs(exact-q8);
	Write($"Numerical Integral (o4a)  : {q[0]}\n");
	Write($"Numerical Integral (o4acc): {Q[0]}\n");
	Write($"Numerical Integral (o8av) : {q8}\n");
	Write($"Analytical Integral       : {exact}\n");
	Write($"Tolerance Goal            : {tol}\n");
	Write($"Estimated Error (o4a)     : {q[1]}\n");
	Write($"Actual Error    (o4a)     : {err}\n");
	Write($"Estimated Error (o4acc)   : {Q[1]}\n");
	Write($"Actual Error    (o4acc)   : {err_cc}\n");
	Write($"Actual Error    (o8av)    : {err_q8}\n");
	Write($"#Evaluations (o4a)        : {count_o4a}\n");
	Write($"#Evaluations (o4acc)      : {count_o4acc}\n");
	Write($"#Evaluations (o8av)       : {count_o8av}\n");
	double sus = 1e-3;
	Write("Without Clenshaw-Curtis variabel transformation:\n");
	test_eval(err,tol,q[1],sus);
	Write("With Clenshaw-Curtis variabel transformation:\n");
	test_eval(err_cc,tol,Q[1],sus);
}//tester

public static void test_eval(double err, double tol,
		double err_est, double sus){
	if(err<=tol){
		if(err_est<err*sus) Write($"Test suspecious: estimated error less than {sus*1e2}% of actual\n");
		else Write("TEST PASSED!\n");
	}//if
	else if(err>err_est) Write("Test suspecious: actual error exceeds estimate.\n");
	else Write("TEST FAILED! Error exceeds tolerance.\n");
}//test_eval
}//main
