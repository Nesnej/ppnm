using System;
using static System.Console;
using static System.Math;
public class main{
public static void Main(){
	int N=0;
	Func<double,double> sqrt=(x) => Sqrt(x);
	double a=0D,b=1D,exact=2D/3;
	double acc=1e-6,eps=1e-6;
	Write($"Integral from {a} to {b} of sqrt(x):\n");
	N += tester(sqrt,a,b,exact,acc,eps);

	Write("\n");

	Func<double,double> sqrt_pol=(x) => 4*Sqrt(1-x*x);
	exact = PI;
	Write($"Integral from {a} to {b} of 4*sqrt(1-x^2):\n");
	N += tester(sqrt_pol,a,b,exact,acc,eps);

	Write("\n");
	if(N==0) Write("ALL TESTS PASSED!\n");
	else Write("NOT ALL TESTS PASSED!\n");
}//Main

public static int tester(Func<double,double> f,
		double a, double b, double exact,
		double acc, double eps){
	double[] q = quad.o4a(f,a,b,acc:acc,eps:eps);
	double Q = dquad.o8a(f,a,b,acc:acc,eps:eps);
	double tol=acc+eps*exact;
	double err = Abs(exact-q[0]);
	double err_o8a = Abs(exact-Q);
	Write($"Numerical Integral (o4a): {q[0]}\n");
	Write($"Numerical Integral (o8a): {Q}\n");
	Write($"Analytical Integral     : {exact}\n");
	Write($"Tolerance Goal          : {tol}\n");
	Write($"Estimated Error (o4a)   : {q[1]}\n");
	Write($"Actual Error    (o4a)   : {err}\n");
	Write($"Actual Error    (o8a)   : {err_o8a}\n");
	double sus = 1e-1;
	if(err<=tol){
		if(q[1]<err*sus) Write($"Test suspecious: estimated error less than {sus*1e2}% of actual\n");
		else {Write("TEST PASSED!\n"); return 0;}
	}//if
	else if(err<q[1]) Write("Test suspecious: actual error exceeds estimate.\n");
	else Write("TEST FAILED! Error exceeds tolerance.\n");
	return 1;
}//tester
}//main
