using System;
using static System.Console;
using static System.Math;
using static System.Double;
public class main{
public static void Main(){
	int count=0,N=0;
	Func<double,double> sqrt=(x) => {count++; return Sqrt(x);};
	double a=0D,b=1D,exact=2D/3;
	double acc=1e-6,eps=1e-6;
	Write($"Integral from {a} to {b} of sqrt(x):\n");
	double[] q = quad.o4a(sqrt,a,b,acc:acc,eps:eps);
	int count_o4a = count; count=0;
	double[] Q = quad.o4acc(sqrt,a,b,acc:acc,eps:eps);
	int count_o4acc = count; count=0;
	double q8 = quad.o8av(sqrt,a,b,acc:acc,eps:eps);
	int count_o8av = count;
	N += tester(q,Q,q8,exact,acc,eps,count_o4a,count_o4acc,count_o8av);

	Write("\n");

	count = 0;
	Func<double,double> sqrt_pol=(x) => {count++; return 4*Sqrt(1-x*x);};
	exact = PI;
	Write($"Integral from {a} to {b} of 4*sqrt(1-x^2):\n");
	q = quad.o4a(sqrt_pol,a,b,acc:acc,eps:eps);
	count_o4a = count; count=0;
	Q = quad.o4acc(sqrt_pol,a,b,acc:acc,eps:eps);
	count_o4acc = count; count=0;
	q8 = quad.o8av(sqrt_pol,a,b,acc:acc,eps:eps);
	count_o8av = count;
	N += tester(q,Q,q8,exact,acc,eps,count_o4a,count_o4acc,count_o8av);

	Write("\n");

	count = 0;
	Func<double,double> sqrt_inv=(x) => {count++; return 1D/Sqrt(x);};
	exact = 2D;
	Write($"Integral from {a} to {b} of 1/sqrt(x):\n");
	q = quad.o4a(sqrt_inv,a,b,acc:acc,eps:eps);
	count_o4a = count; count=0;
	Q = quad.o4acc(sqrt_inv,a,b,acc:acc,eps:eps);
	count_o4acc = count; count=0;
	q8 = quad.o8av(sqrt_inv,a,b,acc:acc,eps:eps);
	count_o8av = count;
	N += tester(q,Q,q8,exact,acc,eps,count_o4a,count_o4acc,count_o8av);

	Write("\n");

	count = 0;
	Func<double,double> sqrt_ln=(x) => {count++; return Log(x)/Sqrt(x);};
	exact = -4; acc=1e-4; eps=1e-4;
	Write($"Integral from {a} to {b} of ln(x)/sqrt(x):\n");
	q = quad.o4a(sqrt_ln,a,b,acc:acc,eps:eps);
	count_o4a = count; count=0;
	Q = //new double[2]{NaN,NaN};
	quad.o4acc(sqrt_ln,a,b,acc:acc,eps:eps);
	count_o4acc = count; count=0;
	q8 = quad.o8av(sqrt_ln,a,b,acc:acc,eps:eps);
	count_o8av = count;
	N+= tester(q,Q,q8,exact,acc,eps,count_o4a,count_o4acc,count_o8av);

	if(N==0) Write("\nALL TESTS PASSED!\n");
	else Write("\nNOT ALL TESTS PASSED!\n");
}//Main

public static int tester(double[] q, double[] Q, double q8,
		double exact, double acc, double eps,
		int count_o4a, int count_o4acc, int count_o8av){
	double tol = acc+eps*Abs(exact);
	double err = Abs(exact-q[0]);
	double err_cc = Abs(exact-Q[0]);
	double err_q8 = Abs(exact-q8);
	Write($"Numerical Integral (o4a)  : {q[0]}\n");
	Write($"Numerical Integral (o4acc): {Q[0]}\n");
	Write($"Numerical Integral (o8av) : {q8}\n");
	Write($"Analytical Integral       : {exact}\n");
	Write($"Tolerance Goal            : {tol}\n");
	Write($"Estimated Error (o4a)     : {q[1]}\n");
	Write($"Actual Error    (o4a)     : {err}\n");
	Write($"Estimated Error (o4acc)   : {Q[1]}\n");
	Write($"Actual Error    (o4acc)   : {err_cc}\n");
	Write($"Actual Error    (o8av)    : {err_q8}\n");
	Write($"#Evaluations (o4a)        : {count_o4a}\n");
	Write($"#Evaluations (o4acc)      : {count_o4acc}\n");
	Write($"#Evaluations (o8av)       : {count_o8av}\n");
	double sus = 1e-1;
	Write("Without Clenshaw-Curtis variabel transformation:\n");
	int n = test_eval(err,tol,q[1],sus);
	Write("With Clenshaw-Curtis variabel transformation:\n");
	n+= test_eval(err_cc,tol,Q[1],sus);
	return n;
}//tester

public static int test_eval(double err, double tol,
		double err_est, double sus){
	if(err<=tol){
		if(err_est<err*sus) Write($"Test suspecious: estimated error less than {sus*1e2}% of actual\n");
		else {Write("TEST PASSED!\n"); return 0;}
	}//if
	else if(err>err_est) Write("Test suspecious: actual error exceeds estimate.\n");
	else Write("TEST FAILED! Error exceeds tolerance.\n");
	return 1;
}//test_eval
}//main
