using System;
using System.IO;
using static System.Console;
using static System.Math;
public class main{
public static void Main(){
	int N=0;
	string[] input = File.ReadAllLines("higgs.data");
	foreach(string line in input){
		N++;
	}//foreach
	vector E = new vector(N);
	vector sigma = new vector(N);
	vector err = new vector(N);
	for(int i=0;i<N;i++){
		string[] s = input[i].Split(' ');
		E[i] = double.Parse(s[0]);
		sigma[i] = double.Parse(s[1]);
		err[i] = double.Parse(s[2]);
	}//foreach

	// The constants from the Breit-Wigner function are labelled as
	// A=x[0], m=x[1] and gamma=x[2].
	Func<double,vector,double> breit_wigner = (e,x) => x[0]/(Pow(e-x[1],2) + x[2]*x[2]/4);
	
	Func<vector,double> lst_square=(y) => {
		double bw=0;
		for(int i=0;i<N;i++){
			bw += Pow(breit_wigner(E[i],y) - sigma[i],2)/err[i]/err[i];
		}//for
		return bw;
	};//Func

	vector z = new vector(5D,120D,1D/100);
	double acc = 1e-2;
	mini.qnewton_sr1(lst_square,ref z,acc);
	var outfile = new StreamWriter("out.higgs.txt");
	outfile.Write("The result of the Breit-Wigner fit is:\n");
	outfile.Write("mass : {0:0.00}\n",z[1]);
	outfile.Write("width: {0:0.00}\n",z[2]);
	outfile.Close();

	for(double en=100;en<160;en+=1D/32){
		Write($"{en} {breit_wigner(en,z)}\n");
	}//for
}//Main
}//main
