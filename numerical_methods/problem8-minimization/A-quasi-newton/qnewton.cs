using System;
using static System.Console;
using static System.Math;
public class mini{
// Square root of machine epsilon on my computer - see the directory mach_eps
public static readonly double EPS = Sqrt(1.11022302462516E-16);

// Function that calculates the gradient of a function in a point
public static vector gradient(Func<vector,double> f, vector x){
	vector g = new vector(x.size);
	double dx, fx = f(x);
	for(int i=0;i<x.size;i++){
		// Setting the stepsize no smaller than machine epsison
		if(Abs(x[i])<Sqrt(EPS)) dx = EPS;
		else dx = Abs(x[i])*EPS;
		x[i] += dx;
		g[i] = (f(x)-fx)/dx;
		x[i] -= dx;
	}//for
	return g;
}//gradient

public static int qnewton_sr1(Func<vector,double> f,
		ref vector x, double acc=1e-3, bool log=false){
	// Setting up the method parameters
	int nsteps=0,n=x.size;
	double lambda_min = 1D/64, alpha=1e-4;
	// Defining gadient and inverse Hessian
	vector g = gradient(f,x);
	matrix B = new matrix(n,n); B.setid();
	// Calculate and perform modified Newton step undtil convergence
	while(g.norm()>acc){
		vector dx = -B*g;
		// Break if the step size becomes too small
		if(dx.norm()<EPS*x.norm()) break;
		double lambda = 1D;
		double fx = f(x);
		double fz = f(x+lambda*dx);
		// Convergence criteria
		double armijo = fx+alpha*lambda*(dx%g);
		while(fz>armijo){
			//Calculating mModified Newton step
			if(lambda>1D/64){
				lambda /= 2;
				fz = f(x+lambda*dx);
			}//if
			else{
				// Resetting inverse Hessian if the the point is bad
				B.setid();
				fz = f(x+lambda*dx);
				if(log){
					Error.Write($"bad point: lambda={lambda_min}\n");
					x.eprint("current point ");
				}//if
				break;
			}//else
		}//while
		// Performing modified Newton step
		x += lambda*dx;
		// Symmetric rank-1 inverse Hessian update
		vector y = gradient(f,x)-g;
		vector u = lambda*dx - B*y;
		if(Abs(u%y)>10*EPS) B.update(u,u,1D/(u%y));
		else B.setid();
		g = gradient(f,x);
		nsteps++;
		// Breaking if maximum number of steps is reached
		if(nsteps==999){
			if(log){
				Error.Write($"Maximum number of steps reached. Exiting at:\n");
				x.eprint();
				Error.Write("\n");
			}//if
			break;
		}//if
	}//while
	return nsteps;
}//qnewton
}//mini
