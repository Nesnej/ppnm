using System;
using static System.Console;
using static System.Math;
public class main{
public static void Main(){
	double acc = 1e-6;
	int N = 0;
	var rnd = new Random();
	Func<vector,double> cos=(x) => Cos(x[0]);
	vector x0 = new vector(.8*PI*(rnd.NextDouble() + .1));
	vector min = x0.copy();
	vector exact = new vector(PI);
	int ncos = mini.qnewton_sr1(cos,ref min,acc);
	Write("Cosine Test:\n");
	Write("If the initial point is close to an extremum, the gradient will be small\n");
	Write("hence the Newton step will be large. It is thus likely, that the method will jump\n");
	Write("tp a different period. This is a consequence of the numerical method, and not a\n");
	Write("fault in the implementation. The test is thus considered passed if it finds any minimum.\n");
	N += tester(cos,x0,min,exact,acc,ncos);

	Write("\n");
	
	Func<vector,double> ho=(x) => x[0]*x[0];
	vector x0ho = new vector(10*(rnd.NextDouble() - .5));
	vector min_ho = x0ho.copy();
	vector exact_ho = new vector(0D);
	int nho = mini.qnewton_sr1(ho,ref min_ho,acc);
	Write("Harmonic Oscillator Test:\n");
	N += tester(ho,x0ho,min_ho,exact_ho,acc,nho);
	
	Write("\n");
	
	Func<vector,double> rosenbrock=(x) => Pow(1-x[0],2) + 100*Pow(x[1]-x[0]*x[0],2);
	vector x0r = new vector(2);
	for(int i=0;i<x0r.size;i++) x0r[i] = 3*(rnd.NextDouble() + .5);
	vector min_r = x0r.copy();
	int nr = mini.qnewton_sr1(rosenbrock,ref min_r,acc);
	vector exact_r = new vector(1D,1D);
	Write("Rosenbrock's Valley Function Test:\n");
	N += tester(rosenbrock,x0r,min_r,exact_r,acc,nr);
	
	Write("\n");
	
	Func<vector,double> himmelblau=(x) => Pow(x[0]*x[0]+x[1]-11,2) + Pow(x[0]+x[1]*x[1]-7,2);
	vector x0h = new vector(2);
	for(int i=0;i<x0h.size;i++) x0h[i] = 10*(rnd.NextDouble() - .5);
	vector min_h = x0h.copy();
	int nh = mini.qnewton_sr1(himmelblau,ref min_h,acc);
	vector exact_h = new vector(2); 
	if((min_h-new vector(3D,2D)).norm()<1D){
		exact_h[0] = 3D; exact_h[1] = 2;}
	else if((min_h-new vector(-2.805118,3.131312)).norm()<1D){
		exact_h[0] = -2.805118; exact_h[1] = 3.131312;}
	else if((min_h-new vector(-3.77931,-3.283186)).norm()<1D){
		exact_h[0] = -3.77931; exact_h[1] = -3.283186;}
	else if((min_h-new vector(3.584428,-1.848126)).norm()<1D){
	       exact_h[0] = 3.584428; exact_h[1] = -1.848126;}
	Write("Himmelblau's Function Test:\n");
	Write("The analytic result is set to the minimum closest to the numerical estimate, since\n");
	Write("a random starting point is used.\n");
	N += tester(himmelblau,x0h,min_h,exact_h,acc,nh);

	Write("\n");
	if(N==0) Write("ALL TESTS PASSED!\n");
	else Write("NOT ALL TESTS PASSED!\n");
	Write("\nOBS: All tests use random numbers to generate the initial condition.\n"
			+"In some cases this might create a bad starting point.\n"
			+"If that is the case just clean the project and rerun make.\n");
}//Main

public static int tester(Func<vector,double> f, vector x0, vector min,
		vector exact, double acc, int nsteps){
	var err = exact - min;
	var dif = Abs(f(exact) - f(min));
	Write($"Starting Point     : ");
	printer(x0);
	Write($"Numerical Minimum  : ");
	printer(min);
	Write($"Analytical Minimum : ");
	printer(exact);
	Write($"Error              : {Abs(err[0])}\n");
	Write($"||f(exact)-f(min)||: {dif}\n");
	Write("Tolerance          : {0:#e0}\n",acc);
	Write($"Number of Steps    : {nsteps}\n");
	if(dif<acc) {Write("TEST PASSED!\n"); return 0;}
	else Write("TEST FAILED!\n");
	return 1;
}//tester

public static void printer(vector v){
	for(int i=0;i<v.size;i++){
		if(i==0) Write($"({v[i]}");
		else if(i==v.size-1) Write($",{v[i]})\n");
		else Write($",{v[i]}");
	}//for
	if(v.size==1) Write(")\n");
}//printer
}//main
