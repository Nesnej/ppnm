using static System.Console;
public class main{
public static void Main(){
	// Calculating the computers machine epsilon
	double eps = 1D;
	while(1D+eps!=1) eps /= 2;
	Write($"{eps}\n");
}//Main
}//main
