using System;
using System.Collections.Generic;
using System.Diagnostics;
using static System.Console;
using static System.Math;
public class smini{
// Method for calculating the centroid of all points but the one with index imax
public static vector cent(List<vector> p, int imax){
	int n = p.Count-1;
	vector pc = new vector(n);
	for(int i=0;i<n+1;i++) if(i!=imax) pc += p[i];
	return pc/n;
}//cent

// Function for finding the index of the largest entry
public static int max_index(vector fs){
	int j=0;
	for(int i=1;i<fs.size;i++) if(fs[i]>fs[j]) j=i;
	return j;
}//max_index

// Function for finding the index of the smallest entry
public static int min_index(vector fs){
	int j=0;
	for(int i=1;i<fs.size;i++) if(fs[i]<fs[j]) j=i;
	return j;
}//min_index

public static int simplex(Func<vector,double> f, ref vector x,
		double acc=1e-3, double dx=1e-2, int limit=999){
	int nsteps=0, n=x.size, N=n+1;
	// List of simplex points
	List<vector> p = new List<vector>();
	// Adding starting point to simplex
	p.Add(x.copy());
	// Adding points with one coordinate displaced by dx to the simplex
	for(int i=0;i<n;i++){
		x[i] += dx;
		p.Add(x.copy());
		x[i] -= dx;
	}//for
	double dist=0;
	// Calculating the sum of distances between points
	for(int i=0;i<N;i++) dist += (p[(i+1)%N]-p[i]).norm();
	vector fs = new vector(N);
	// Calculating function values for each simplex point
	for(int i=0;i<N;i++) fs[i] = f(p[i]);
	int imin,imax;
	// Move the simplex as long as the average distance between points exceeds the accurary goal
	while(dist/n>acc){
		// Finding the simplex points with the smallest and largest function values
		imax = max_index(fs);
		vector phi = p[imax];
		imin = min_index(fs);
		vector pl = p[imin];
		// Calculating centroid
		vector pc = cent(p,imax);
		// Storing reflected point and function value
		vector pref = 2*pc-phi;
		double fref = f(pref);
		// Checking if the reflected point function value is smaller than the smallest in the simplex
		if(fref < f(pl)){
			// Try expansion
			if(f(3*pc-2*phi) < fref) p[imax] = 3*pc-2*phi;
			// Otherwise accept reflection
			else p[imax] = pref;
			// Store the function value of the new simplex point
			fs[imax] = f(p[imax]);
		}//if
		else{
			// If the reflected point is better than the higest simplex point then accept it
			if(fref < f(phi)){
				p[imax] = pref;
				// Store the function value of the new simplex point
				fs[imax] = f(p[imax]);
			}//if
			else{
				// Try contraction
				if(f((phi+pc)/2) < fref){
					p[imax] = (phi+pc)/2;
					// Store the function value of the new simplex point
					fs[imax] = f(p[imax]);
				}//if
				else{
					// Perform reduction of the simplex
					for(int i=0;i<imin;i++){
						p[i] = (p[i] + pl)/2;
						fs[i] = f(p[i]);
					}//for
					for(int i=imin+1;i<n+1;i++){
						p[i] = (p[i] + pl)/2;
						fs[i] = f(p[i]);
					}//for
				}//else
			}//else
		}//else
		// Recalculate the sum of distances between points
		dist=0;
		for(int i=0;i<N;i++) dist += (p[(i+1)%N]-p[i]).norm();
		// If the number of simplex steps exceeds the limit break the loop
		nsteps++;
		if(nsteps==limit){
			Error.Write("Maximum number of steps reached\n");
			break;
		}//if
	}//while
	// Use the simplex point of smallest function value as estimate of the minimum
	imin = min_index(fs);
	x = p[imin];
	return nsteps;
}//simplex
}//mini
