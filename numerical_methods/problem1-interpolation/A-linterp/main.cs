using System;
using static System.Console;
using static System.Math;
class main{
static void Main(){
	int N = 5, n = 200;
	double[] x = new double[N];
	double[] y = new double[N];

	int i;
	for(i=0;i<N;i++){
		x[i] = 2*PI*i/(N-1);
		y[i] = Cos(x[i]);
		WriteLine($"{x[i]} {y[i]}");
	}//for

	Write("\n\n");

	var lt = new linterp(x,y);
	double z=x[0], dz=(x[N-1]-x[0])/(n-1);
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {Cos(z)} {lt.eval(z)}");
	}//for

	Write("\n\n");

	z=x[0];
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {Sin(z)} {lt.integ(z)}");
	}//for

	Write("\n\n");

	double[] int_tab = new double[N];
	int_tab[0] = 0;
	int_tab[2] = 0;
	int_tab[N-1] = 0;
	int_tab[1] = PI/4;
	int_tab[3] = -PI/4;
//	WriteLine($"{0} {int_tab[0]}");
	for(i=0;i<N;i++){
////		int_tab[i] = int_tab[i-1] - (y[i]-y[i-1])/((x[i]-x[i-1]))*Pow(-1,;
//		int_tab[i] = int_tab[i-1] + Pow(-1,i)*PI/4;//(y[i]-y[i-1])/((x[i]-x[i-1]));
//		Error.Write($"x={x[i]-x[i-1]}, y={y[i]-y[i-1]}\n");
		WriteLine($"{2*PI*i/(N-1)} {int_tab[i]}");
	}//for
}//Main
}//main
