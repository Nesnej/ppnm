using System;
using static System.Math;
using static System.Console;
class main{
static void Main(){
	int n = 6;
	double[] x = new double[n];
	for(int i=0; i<n; i++) {x[i] = i;}
	int N = 4;
	double[] y = new double[N];
	y[0] = .5; y[1] = 1; y[2] = Sqrt(2); y[3] = 3.5;
	int[] k = new int[N];
	int N_test = 0;
	Write("Binary search test:\n");
	for(int j=0; j<N; j++) {
		k[j] = linterp.bin_search(x,y[j]);
		if(x[k[j]] < y[j] && x[k[j]+1] >= y[j]){
			WriteLine($"{x[k[j]]}<{y[j]}<={x[k[j]+1]}");
			WriteLine($"Test {j} passed.");
			N_test++;
		}//if
		else{
			WriteLine($"Test {j} failed.");
		}//else
	}//for
	if(N-N_test == 0) WriteLine("All tests passed.");
	else WriteLine("Not all tests passed.");
}//Main
}//main
