using System;
using static System.Console;
using static System.Math;
class main{
static void Main(){
	int N = 5, n = 200;
	double[] x = new double[N];
	double[] y = new double[N];

	int i;
	for(i=0;i<N;i++){
		x[i] = 2*PI*i/(N-1);
		y[i] = Cos(x[i]);
		WriteLine($"{x[i]} {y[i]}");
	}//for

	Write("\n\n");

	var qs = new qspline(x,y);
	double z=x[0], dz=(x[N-1]-x[0])/(n-1);
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {Cos(z)} {qs.eval(z)}");
	}//for

	Write("\n\n");

	z=x[0];
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {Sin(z)} {qs.integ(z)}");
	}//for

	Write("\n\n");

	z=x[0];
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {-Sin(z)} {qs.deriv(z)}");
	}//for
}//Main
}//main
