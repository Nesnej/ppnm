using System;
using static System.Console;
using static System.Math;
class main{
static void Main(){
	int N = 5, n = 200;
	double[] x = new double[N];
	double[] y = new double[N];
	double[] w = new double[N];

	int i;
	for(i=0;i<N;i++){
		x[i] = 2*PI*i/(N-1);
		y[i] = Sin(x[i]);
		w[i] = Cos(x[i]);
		WriteLine($"{x[i]} {y[i]} {w[i]}");
	}//for

	Write("\n\n");

	var cs_sin = new qspline(x,y);
	var cs_cos = new qspline(x,w);
	double z=x[0], dz=(x[N-1]-x[0])/(n-1);
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {Sin(z)} {cs_sin.eval(z)} {Cos(z)} {cs_cos.eval(z)}");
	}//for

	Write("\n\n");

	z=x[0];
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {1-Cos(z)} {cs_sin.integ(z)} {Sin(z)} {cs_cos.integ(z)}");
	}//for 

	Write("\n\n");

	z=x[0];
	for(i=0;i<n;i++){
		z += dz;
		WriteLine($"{z} {Cos(z)} {cs_sin.deriv(z)} {-Sin(z)} {cs_cos.deriv(z)}");
	}//for
}//Main
}//main
