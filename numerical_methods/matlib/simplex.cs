using System;
using System.Collections.Generic;
using System.Diagnostics;
using static System.Console;
using static System.Math;
public class smini{
public static vector cent(List<vector> p, int imax){
	int n = p.Count-1;
	vector pc = new vector(n);
	for(int i=0;i<n+1;i++) if(i!=imax) pc += p[i];
	return pc/n;
}//cent

public static int max_index(vector fs){
	int j=0;
	for(int i=1;i<fs.size;i++) if(fs[i]>fs[j]) j=i;
	return j;
}//max_index

public static int min_index(vector fs){
	int j=0;
	for(int i=1;i<fs.size;i++) if(fs[i]<fs[j]) j=i;
	return j;
}//min_index

public static double dist_calc(List<vector> p, int i){
	int n = p.Count;
	if(i==-1) i=n-1;
	return (p[(i+1)%n]-p[i]).norm();
}//dist_calc

public static int simplex(Func<vector,double> f, ref vector x,
		double acc=1e-3, double dx = 1e-2){
	int nsteps=0,n=x.size,N=n+1;
	List<vector> p = new List<vector>();
	p.Add(x.copy());
	for(int i=0;i<n;i++){
		x[i] += dx;
		p.Add(x.copy());
		x[i] -= dx;
	}//for
	double dist=0;
	for(int i=0;i<N;i++) dist += dist_calc(p,i);
	vector fs = new vector(N);
	for(int i=0;i<N;i++) fs[i] = f(p[i]);
	int imin,imax;
	while(dist>acc){
		imax = max_index(fs);
		vector phi = p[imax];
		imin = min_index(fs);
		vector pl = p[imin];
		vector pc = cent(p,imax);
		vector pref = 2*pc-phi;
		double fref = f(pref);
		if(fref < f(pl)){
			if(n>=5) for(int i=imax-1;i<imax+1;i++) dist -= dist_calc(p,i);
			if(f(3*pc-2*phi) < fref) p[imax] = 3*pc-2*phi;
			else p[imax] = pref;
			fs[imax] = f(p[imax]);
			if(n>=5) for(int i=imax-1;i<imax+1;i++) dist += dist_calc(p,i);
		}//if
		else{
			if(fref < f(phi)){
				if(n>=5) for(int i=imax-1;i<imax+1;i++) dist -= dist_calc(p,i);
				p[imax] = pref;
				fs[imax] = f(p[imax]);
				if(n>=5) for(int i=imax-1;i<imax+1;i++) dist += dist_calc(p,i);
			}//if
			else{
				if(f((phi+pc)/2) < fref){
					if(n>=5) for(int i=imax-1;i<imax+1;i++) dist -= dist_calc(p,i);
					p[imax] = (phi+pc)/2;
					fs[imax] = f(p[imax]);
					if(n>=5) for(int i=imax-1;i<imax+1;i++) dist += dist_calc(p,i);
				}//if
				else{
					for(int i=0;i<imin;i++){
						p[i] = (p[i] + pl)/2;
						fs[i] = f(p[i]);
					}//for
					for(int i=imin+1;i<n+1;i++){
						p[i] = (p[i] + pl)/2;
						fs[i] = f(p[i]);
					}//for
					if(n>=5) for(int i=0;i<N;i++) dist += dist_calc(p,i);
				}//else
			}//else
		}//else
		if(n<5){
			dist=0;
			for(int i=0;i<N;i++) dist += dist_calc(p,i);
		}//if
		nsteps++;
		if(nsteps==999){
			Error.Write("Maximum number of steps reached\n");
			break;
		}//if
	}//while
	imin = min_index(fs);
	x = p[imin];
	return nsteps;
}//simplex
}//mini
