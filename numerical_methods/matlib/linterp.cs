using System;
using static System.Console;
public class linterp{
	double[] x,y,p;

// Binary search method
public static int bin_search(double[] x, double z){
	int i = 0;
	int j = x.Length-1;
	while(j-i>1){
		int m = (i+j)/2;
		if(z>x[m]){i=m;}
		else{j=m;}
	}//while
	return i;
}//binsearch

public linterp(double[] xs, double[] ys){
	int n = xs.Length;
	// Error is unequal number x and y values
	System.Diagnostics.Trace.Assert(ys.Length>=n);
	x = new double[n];
	y = new double[n];
	for(int i=0;i<n;i++){x[i]=xs[i];y[i]=ys[i];}
	p = new double[n-1];
	for(int i=0;i<n-1;i++){
		p[i] = (y[i+1] - y[i])/(x[i+1] - x[i]);
	}//for
}//linterp - constructor

public double eval(double z){
	// Error if function evaluation is attempted outside the interpolated interval
	System.Diagnostics.Trace.Assert(z>=x[0] && z<=x[x.Length-1]);
	int i = bin_search(x,z);
	double dx = z-x[i];
	return y[i] + dx*p[i];
	}//eval

public double integ(double z){
	int i = bin_search(x,z);
	double sum = 0;
	for(int j=0;j<i;j++){
		sum += p[j]*(x[j+1]*x[j+1] - x[j]*x[j])/2 + (y[j]-p[j]*x[j])*(x[j+1] - x[j]);
	}//for
	return sum+=p[i]*(z*z - x[i]*x[i])/2 + (y[i]-p[i]*x[i])*(z - x[i]);
}//integ
}//cspline
