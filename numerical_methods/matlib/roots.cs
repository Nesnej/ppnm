using System;
using static System.Math;
using static System.Console;
public class roots{
public static vector newton(Func<vector,vector> f, vector x,
		double acc=1e-3, double dx=1e-7){
	int n=x.size, niter=0;
	double lambda_min = 1D/64;
	// Setting up gradient and point for function evaluation
	matrix J = new matrix(n,n);
	vector root = x.copy();
	vector fr = f(root);
	vector z,fz;
	// Do Newton step until convergence
	while(fr.norm()>acc){
		if(niter>100){
			Error.Write($"The number of iterations exceeded the maximum allowed at {niter}\n");
			break;
		}//if
		// Calculating gradient
		for(int j=0;j<n;j++){
			root[j] += dx;
			vector df = f(root)-fr;
			for(int i=0;i<n;i++) J[i,j] = df[i]/dx;
			root[j] -= dx;
		}//for
		// Calculating Newton full step
		var qr = new gramschmidt(J);
		var dr = qr.solve(-1*fr);
		double lambda = 1D;
		z = root+lambda*dr;
		fz = f(z);
		// Doing modified Newton step
		while(fz.norm()>(1-lambda/2)*fr.norm()){
			if(lambda>lambda_min){
				lambda /= 2;
				fz = f(root+lambda*dr);
			}//if
			// Breaking if too many step attempts are bad
			else {Error.Write($"bad point: lambda={lambda_min}\n"); break;}
		}//while
		// Updating root estimate
		root += lambda*dr;
		fr = f(root);
		niter++;
	}//while
	return root;
}//newton

public static double newton(Func<double,double> f, double x,
		double acc=1e-3, double dx=1e-7){
	double lambda_min = 1D/64;
	// Setting up derivative and point for function evaluation
	double fx = f(x);
	double z,fz,J;
	// Do Newton step until convergence
	while(Abs(fx)>acc){
		// Calculating derivative
		J = (f(x+dx)-fx)/dx;
		// Calculating Newton full step
		double lambda = 1D;
		z = x-lambda*fx/J;
		fz = f(z);
		// Doing modified Newton step
		while(Abs(fz)>(1-lambda/2)*Abs(fx)){
			if(lambda>lambda_min){
				lambda /= 2;
				fz = f(x-lambda*fx/J);
			}//if
			// Breaking if too many step attempts are bad
			else {Error.Write($"bad point: lambda={lambda_min}\n"); break;}
		}//while
		// Updating root estimate
		x -= lambda*fx/J;
		fx = f(x);
	}//while
	return x;
}//newton
}//root
