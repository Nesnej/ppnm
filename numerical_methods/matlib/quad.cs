using System;
using static System.Math;
using static System.Double;
public static class quad{
public static double[] o4av(Func<double,double> f,
		double a, double b, int lim=99,
		double acc=1e-6, double eps=1e-6){
	// Putting integration limits in correct order
	if(a>b) return o4av(f,a,b,lim,acc,eps);
	// Transformation for negative infinity as lower limit
	else if(IsNegativeInfinity(a) && !IsInfinity(b)){
		return o4a(t=>f(b-(1-t)/t)/t/t,0,1,lim,acc,eps);
	}//if
	// Transformation for infinity as upper limit
	else if(!IsInfinity(a) && IsPositiveInfinity(b)){
		return o4a(t=>f(a+(1-t)/t)/t/t,0,1,lim,acc,eps);
	}//else if
	// Tranformation for integral over all real numbers
	else if(IsNegativeInfinity(a) && IsPositiveInfinity(b)){
		return o4a(t=>(f((1-t)/t) + f((t-1)/t))/t/t,0,1,lim,acc,eps);
	}//else if
	// Otherwise just regular o4a
	else return o4a(f,a,b,lim,acc,eps);
}//o4av

public static double[] o4a(Func<double,double> f,
		double a, double b, int lim=99,
		double acc=1e-6, double eps=1e-6,
		double f2=NaN, double f3=NaN){
	double dx = b-a;
	double f1 = f(a+dx/6), f4 = f(b-dx/6);
	// Initial points number 2 and 3
	if(IsNaN(f2)) f2 = f(a+2*dx/6);
	if(IsNaN(f3)) f3 = f(b-2*dx/6);
	// Integral estimate
	double Q = (2*f1+f2+f3+2*f4)/6*dx;
	// Lower order integral estimate
	double q = (f1+f2+f3+f4)/4*dx;
	// Error proportional to absolute difference in estimates
	double err = Abs(Q-q)/1.4;
	// Checking if the maximum number of points has been reached
	if(lim==0){
		Console.Error.Write($"limit reached: a={a}, b={b}\n");
		double[] res = {Q,err};
		return res;
	}//if
	// Checking convergence
	if(err<acc+eps*Abs(Q)){
		double[] res = {Q,err};
		return res;
	}//if
	else{
		// Splitting the interval
		double[] r1=o4a(f,a,(a+b)/2,acc:acc/Sqrt(2),eps:eps,lim:lim-1,f2:f1,f3:f2);
		double[] r2=o4a(f,(a+b)/2,b,acc:acc/Sqrt(2),eps:eps,lim:lim-1,f2:f3,f3:f4);
		Q = r1[0] + r2[0];
		err = Sqrt(r1[1]*r1[1] + r2[1]*r2[1]);
		double[] res = {Q,err};
		return res;
	}//else
}//o8a

public static double[] o4acc(Func<double,double> f,
		double a, double b, int lim=99,
		double acc=1e-6, double eps=1e-6){
	// Applying Clenshaw-Curtis variable transformation
	Func<double,double> F=(x) => f((a+b + (b-a)*Cos(x))/2)*Sin(x)*(b-a)/2;
	// Calculate the transformed integral with ordinaty o4a
	return o4a(F,0,PI,lim,acc,eps);
}//o4acc
}//quad
