using System;
using System.Diagnostics;
using static System.Math;
public partial class jacobi{
public static int cyclic(matrix A, vector e, matrix V){
	bool changed=true; int sweeps=0, n=A.size1;
	// Setting up e and V
	for(int i=0;i<n;i++){
		e[i] = A[i,i];
		V[i,i] = 1D;
		for(int j=i+1;j<n;j++){
			V[i,j] = 0D;
			V[j,i] = V[i,j];
		}//for
	}//for
	while(changed){
		// Performing sweep
		changed=false;
		for(int q=n-1;q>0;q--){
			for(int p=0;p<q;p++){
				sweeps++;
				// Setting the matrix element p,q to 0 by Givens rotation
				changed = rotation(A,V,e,p,q);
			}//for
		}//for
	}//while
	return sweeps;
}//cyclic

public static int cyclic(matrix A, vector e, matrix V,int neig,bool low=true){
	int sweeps=0, n=A.size1;
	// Setting up e and V
	for(int i=0;i<n;i++){
		e[i] = A[i,i];
		V[i,i] = 1D;
		for(int j=i+1;j<n;j++){
			V[i,j] = 0D;
			V[j,i] = V[i,j];
		}//for
	}//for
	for(int p=0;p<neig;p++){
		// Performing sweep
		bool changed=true;
		while(changed){
			changed=false;
			for(int q=p+1;q<n;q++){
				sweeps++;
				// Setting the matrix element p,q to 0 by Givens rotation
				changed = rotation(A,V,e,p,q,low);
			}//for
		}//while
	}//for
	return sweeps;
}//cyclic

public static bool rotation(matrix A, matrix V, vector e, int p, int q,bool low=true){
	double app = e[p], aqq = e[q], apq = A[p,q];
	double phi;
	if(low) phi = Atan2(2*apq,aqq-app)/2;
	else phi = Atan2(-2*apq,-aqq+app)/2;
	double c = Cos(phi), s = Sin(phi);
	double app1 = c*c*app - 2*s*c*apq + s*s*aqq;
	double aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
	int n=A.size1;
	// Applying rotation
	if(app1!=app || aqq1!=aqq){
		e[q] = aqq1; e[p] = app1; A[p,q] = 0D;
		for(int i=0;i<p;i++){
			double aip = A[i,p], aiq = A[i,q];
			A[i,p] = c*aip - s*aiq;
			A[i,q] = s*aip + c*aiq;
		}//for
		for(int i=p+1;i<q;i++){
			double aip = A[p,i], aiq = A[i,q];
			A[p,i] = c*aip - s*aiq;
			A[i,q] = s*aip + c*aiq;
		}//for
		for(int i=q+1;i<n;i++){
			double aip = A[p,i], aiq = A[q,i];
			A[p,i] = c*aip - s*aiq;
			A[q,i] = s*aip + c*aiq;
		}//for
		// Updating the eigenvectors
		if(V!=null)for(int i=0;i<n;i++){
			double vip = V[i,p], viq = V[i,q];
			V[i,p] = c*vip - s*viq;
			V[i,q] = s*vip + c*viq;
		}//for
		return true;
	}//if
	else return false;
}//sweep

public static int classic(matrix A, vector e, matrix V){
	bool changed=true; int nrot=0, n=A.size1;
	double app,apq,aqq,phi,c,s,app1,aqq1,aip,aiq,vip,viq;
	// Setting up e and V
	for(int i=0;i<n;i++){
		e[i] = A[i,i];
		V[i,i] = 1D;
		for(int j=i+1;j<n;j++){
			V[i,j] = 0D;
			V[j,i] = V[i,j];
		}//for
	}//for
	// Finding the index of the largest absolute value for each row
	int[] imaxs = new int[n-1];
	for(int i=0;i<n-1;i++){
		int k = i+1;
		for(int j=i+2;j<n-1;j++){
			if(Abs(A[i,j])>Abs(A[i,k])) k = j;
		}//for
		imaxs[i] = k;
	}//for
	while(changed){
		nrot++; changed=false;
		// Finding the index of the largest absolute value
		int p = 0;
		for(int i=1;i<n-1;i++){
			if(Abs(A[i,imaxs[i]])>Abs(A[p,imaxs[p]])) p = i;
		}//for
		int q = imaxs[p];
		// Performing Givens rotation on that element
		app = e[p] ; aqq = e[q]; apq = A[p,q];
		phi = Atan2(2*apq,aqq-app)/2;
		c = Cos(phi); s = Sin(phi);
		app1 = c*c*app - 2*s*c*apq + s*s*aqq;
		aqq1 = s*s*app + 2*s*c*apq + c*c*aqq;
		if(app1!=app || aqq1!=aqq){
			changed = true; e[q] = aqq1; e[p] = app1; A[p,q] = 0D;
			for(int i=0;i<p;i++){
				aip = A[i,p]; aiq = A[i,q];
				A[i,p] = c*aip - s*aiq;
				if(Abs(A[i,p])>Abs(A[i,imaxs[i]])) imaxs[i] = p;
				A[i,q] = s*aip + c*aiq;
				if(Abs(A[i,q])>Abs(A[i,imaxs[i]])) imaxs[i] = q;
			}//for
			for(int i=p+1;i<q;i++){
				aip = A[p,i]; aiq = A[i,q];
				A[p,i] = c*aip - s*aiq;
				if(Abs(A[p,i])>Abs(A[p,imaxs[p]])) imaxs[p] = i;
				A[i,q] = s*aip + c*aiq;
				if(Abs(A[i,q])>Abs(A[i,imaxs[i]])) imaxs[i] = q;
			}//for
			for(int i=q+1;i<n;i++){
				aip = A[p,i]; aiq = A[q,i];
				A[p,i] = c*aip - s*aiq;
				if(Abs(A[p,i])>Abs(A[p,imaxs[p]])) imaxs[p] = i;
				A[q,i] = s*aip + c*aiq;
				if(Abs(A[q,i])>Abs(A[q,imaxs[q]])) imaxs[q] = i;
			}//for
			// Updating the eigenvectors
			if(V!=null)for(int i=0;i<n;i++){
				vip = V[i,p]; viq = V[i,q];
				V[i,p] = c*vip - s*viq;
				V[i,q] = s*vip + c*viq;
			}//for
		}//if
	}//while
	return nrot;
}//classic
}//jacobi
