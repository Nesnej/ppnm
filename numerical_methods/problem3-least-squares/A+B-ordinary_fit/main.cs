using System;
using static System.Console;
using static System.Math;
class main{
public static void Main(){
	// calling the constructor of the vector class in such a way, that the data is easily filled in - see source code
	vector x_in = new vector(new double[]{1,  2,  3, 4, 6, 9,   10,  13,  15});
	vector y_in = new vector(new double[]{117,100,88,72,53,29.5,25.2,15.2,11.1});
	vector dy_in = y_in/20;
	int n = x_in.size;
	vector x = new vector(n);
	vector y = new vector(n);
	vector dy = new vector(n);
	for(int i=0;i<n;i++){
		x[i] = x_in[i];
		y[i] = Log(y_in[i]);
		dy[i] = dy_in[i]/y_in[i];
		Write($"{x_in[i]} {y_in[i]} {dy_in[i]} {y[i]} {dy[i]}\n");
	}//for
	Write("\n\n");
	//The fit model is exponential decay thus the logarithmic function is a constant plus a linear function
	var fs = new Func<double,double>[]{z=>1,z=>z};
	var fit = new lsfit(x,y,dy,fs);
	double lambda = fit.c[1];
//	double a = Exp(fit.c[0]);
	double dlambda = Sqrt(fit.sigma[1,1]);
	double T = Log(2)/cmath.abs(lambda);
	double dT = Log(2)*cmath.abs(dlambda/(lambda*lambda));
	double dt = 1D/32;
	for(double t=x[0];t<x[n-1];t+=dt){
		Write($"{t} {Exp(fit.eval(t))} {fit.eval(t)}\n");
	}//for
	Write("\n\n");
	fit.c[0] -= Sqrt(fit.sigma[0,0]);
	for(double t=x[0];t<x[n-1];t+=dt){
		Write($"{t} {fit.eval(t)}\n");
	}//for

	Write("\n\n");
	fit.c[0] += 2*Sqrt(fit.sigma[0,0]);
	for(double t=x[0];t<x[n-1];t+=dt){
		Write($"{t} {fit.eval(t)}\n");
	}//for

	Write("\n\n");
	fit.c[0] -= Sqrt(fit.sigma[0,0]);
	fit.c[1] -= Sqrt(fit.sigma[1,1]);
	for(double t=x[0];t<x[n-1];t+=dt){
		Write($"{t} {fit.eval(t)}\n");
	}//for
	
	Write("\n\n");
	fit.c[1] += 2*Sqrt(fit.sigma[1,1]);
	for(double t=x[0];t<x[n-1];t+=dt){
		Write($"{t} {fit.eval(t)}\n");
	}//for
	Error.Write($"half-life fit: {T:f2} +- {dT:f2} days\n");
	Error.Write($"half-life table: 3.62 +- 0.01 days\n");
}//Main
}//main
