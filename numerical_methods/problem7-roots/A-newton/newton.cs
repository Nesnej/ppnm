using System;
using static System.Console;
public class roots{
public static vector newton(Func<vector,vector> f, vector x,
		double acc=1e-3, double dx=1e-7){
	int n=x.size;
	double lambda_min = 1D/64;
	// Setting up gradient and point for function evaluation
	matrix J = new matrix(n,n);
	vector root = x.copy();
	vector fr = f(root);
	vector z,fz;
	// Do Newton step until convergence
	while(fr.norm()>acc){
		// Calculating gradient
		for(int j=0;j<n;j++){
			root[j] += dx;
			vector df = f(root)-fr;
			for(int i=0;i<n;i++) J[i,j] = df[i]/dx;
			root[j] -= dx;
		}//for
		// Calculating Newton full step
		var qr = new gramschmidt(J);
		var dr = qr.solve(-1*fr);
		double lambda = 1D;
		z = root+lambda*dr;
		fz = f(z);
		// Doing modified Newton step
		while(fz.norm()>(1-lambda/2)*fr.norm()){
			if(lambda>lambda_min){
				lambda /= 2;
				fz = f(root+lambda*dr);
			}//if
			// Breaking if too many step attempts are bad
			else {Error.Write($"bad point: lambda={lambda_min}\n"); break;}
		}//while
		// Updating root estimate
		root += lambda*dr;
		fr = f(root);
	}//while
	return root;
}//newton
}//root
