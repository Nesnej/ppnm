using System;
using static System.Console;
using static System.Math;
public static class main{
public static void Main(){
	var rnd = new Random(1);
	int N=0;
	double acc = 1e-12;
	Func<vector,vector> ho1d=(x) => new vector(x[0]*x[0]);
	vector x01d = new vector(5*(rnd.NextDouble() - .5));
	vector root1d = roots.newton(ho1d,x01d,acc);
	vector exact1d = new vector(0D);
	Write("1D Harmonic Oscillator Test:\n");
	N += tester(ho1d,x01d,root1d,exact1d,acc);
	
	Write("\n");

	Func<vector,vector> sin=(x) => new vector(Sin(x[0]));
	vector x0s = new vector(1D);
	vector root_s = roots.newton(sin,x0s,acc);
	vector exacts = new vector(0D);
	Write("1D Sine Test:\n");
	N += tester(sin,x0s,root_s,exacts,acc);
	if(Abs(exacts[0]-root_s[0])>acc && Abs(root_s[0]-exacts[0])%PI<acc){
		Write("OBS: A different root than the nearest has been found!\n");
		Write($"Absolute Error mod pi : {Abs(root_s[0]-exacts[0])%PI}\n");
	}//if

	Write("\n");

	Func<vector,vector> cos=(x) => new vector(Cos(x[0]));
	vector exactc = new vector(-PI/2);
	vector x0c = new vector(-.1);
	for(int i=0;i<2;i++){
		vector rootc = roots.newton(cos,x0c,acc);
		Write("1D Cosine Test:\n");
		N += tester(cos,x0c,rootc,exactc,acc);
		if(Abs(exactc[0]-rootc[0])>acc && Abs(rootc[0]-exactc[0])%PI<acc){
			Write("OBS: A different root than the nearest has been found!\n");
			Write($"Absolute Error mod pi : {Abs(rootc[0]-exactc[0])%PI}\n");
		}//if
		x0c *= 10;
		Write("\n");
	}//for

	Func<vector,vector> ho2d=(x) => new vector(x[0]*x[0],x[1]*x[1]);
	vector x02d = new vector(2);
	for(int i=0;i<x02d.size;i++) x02d[i] = 5*(rnd.NextDouble() - .5);
	vector root2d = roots.newton(ho2d,x02d,acc);
	vector exact2d = new vector(0D,0D);
	Write("2D Harmonic Oscillator Test:\n");
	N += tester(ho2d,x02d,root2d,exact2d,acc);

	Write("\n");

	double a = 1D;
	Func<vector,vector> gauss=(x) => new vector(1-Exp(-x[0]*x[0]),(x[1]-a)*(x[1]-a));
	vector x0g = new vector(2);
	for(int i=0;i<x0g.size;i++) x0g[i] = 5*(rnd.NextDouble() - .5);
	vector rootg = roots.newton(gauss,x0g,acc);
	vector exactg = new vector(0D,a);
	Write("Nonharmonic Oscillator Test:\n");
	Write("f(x,y) = (1-exp(-x^2),(y-1)^2)\n");
	N += tester(gauss,x0g,rootg,exactg,acc);
	
	Write("\n");

	Func<vector,vector> rosenbrock=(x) => {return 
		new vector(-2*(1-x[0]+200*x[0]*(x[1]-x[0]*x[0])),
				200*(x[1]-x[0]*x[0]));
				};//Func
	vector x0r = new vector(2);
	for(int i=0;i<x0r.size;i++) x0r[i] = 5*(rnd.NextDouble() - .5);
	vector rootr = roots.newton(rosenbrock,x0r,acc);
	vector exactr = new vector(1D,1D);
	Write("Rosenbrock's Valley Function Test:\n");
	N += tester(rosenbrock,x0r,rootr,exactr,acc);

	Write("\n");
	if(N==0) Write("ALL TESTS PASSED!\n");
	else Write("NOT ALL TESTS PASSED!\n");
}//Main

public static int tester(Func<vector,vector> f, vector x0, vector root, vector exact, double acc){
	var err = exact - root;
	var dif = f(exact) - f(root);
	Write($"Starting Point : ");
	for(int i=0;i<x0.size;i++){
		if(i==0) Write($"({x0[i]}");
		else if(i==x0.size-1) Write($",{x0[i]})\n");
		else Write($",{x0[i]}");
	}//for
	if(x0.size==1) Write(")\n");
	Write($"Numerical Root : ");
	for(int i=0;i<root.size;i++){
		if(i==0) Write($"({root[i]}");
		else if(i==root.size-1) Write($",{root[i]})\n");
		else Write($",{root[i]}");
	}//for
	if(root.size==1) Write(")\n");
	Write($"Analytical Root: ");
	for(int i=0;i<exact.size;i++){
		if(i==0) Write($"({exact[i]}");
		else if(i==exact.size-1) Write($",{exact[i]})\n");
		else Write($",{exact[i]}");
	}//for
	if(exact.size==1) Write(")\n");
	double crit;
	if(root.size==1){
		Write($"Error          : {Abs(err[0])}\n");
		Write($"||f(root)||    : {Abs(dif[0])}\n");
		crit = dif[0];
	}//if
	else{
		Write($"Numerical Error: {err.norm()}\n");
		Write($"||f(root)||    : {dif.norm()}\n");
		crit = dif.norm();
	}//else
	Write($"Tolerance      : {acc}\n");
	if(crit<acc) {Write("TEST PASSED!\n"); return 0;}
	else Write("TEST FAILED!\n");
	return 1;
}//tester
}//main
