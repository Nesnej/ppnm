using System;
using System.Collections.Generic;
using static System.Console;
using static System.Math;
public static class main{
public static void Main(string[] args){
	double rmax = 10, eps = 1e-6;
	bool test = true;
	foreach(string str in args){
		Error.Write($"str: "+str);
		string[] ws = str.Split('=');
		if(ws[0]=="rmax") rmax=double.Parse(ws[1]);
		if(ws[0]=="eps") eps=double.Parse(ws[1]);
		if(ws[0]=="test") test=bool.Parse(ws[1]);
	}//foreach
	Func<vector,vector> aux = (v) => new vector(swave(v[0],rmax,eps));

	var rnd = new Random();
	vector E0 = new vector(rnd.NextDouble() - 1.4);
	vector E = roots.newton(aux,E0);
	
	List<double> rs = new List<double>();
	List<vector> fs = new List<vector>();
	double r0=1e-3;
	vector f0 = new vector(r0*(1-r0),1-2*r0);
	Func<double,vector,vector> diff_eq=(x,f) => new vector(f[1],-2*(1D/x+E[0])*f[0]);
	vector psi_max = ode.rk45(diff_eq,r0,f0,rmax,xlist:rs,ylist:fs,acc:eps,eps:0,h:1e-3);
	vector psi_raw = new vector(rs.Count);
	vector r_raw = new vector(rs.Count);
	for(int i=0;i<rs.Count;i++){
		r_raw[i] = rs[i];
		psi_raw[i] = fs[i][0];
	}//for
	var psi = new cspline(r_raw,psi_raw);
	for(double r=0;r<rmax;r+=1D/64) Write($"{r} {psi.eval(r)} {r*Exp(-r)}\n");
	Write("\n\n");

	var efile = new System.IO.StreamWriter("out.energy.txt", append: true);
	efile.Write($"{rmax} {E[0]}\n");
	efile.Close();

	if(test){
		vector exact = new vector(-1D/2);
		var err = exact - E;
		double assymp = Exp(-Sqrt(2*Abs(E[0]))*rmax);
		var dif = psi_max[0] - assymp;
		var outfile = new System.IO.StreamWriter("out.test.txt", append: true);
		outfile.Write($"rmax                : {rmax}\n");
		outfile.Write($"Starting Energy     : {E0[0]}\n");
		outfile.Write($"Numerical Energy    : {E[0]}\n");
		outfile.Write($"Analytical Energy   : {exact[0]}\n");
		outfile.Write($"Numerical Error     : {Abs(err[0])}\n");
		outfile.Write($"||f(rmax)-assymp||  : {Abs(dif)}\n");
		outfile.Write($"Assymptotic Solution: Exp(-Sqrt(2*Abs(E))*rmax)\n");
		outfile.Write($"Tolerance           : {eps}\n");
		if(E[0]-exact[0]<eps) outfile.Write("TEST PASSED!\n");
		else outfile.Write("TEST FAILED!\n");
		outfile.Close();
	}//if
}//Main

public static double swave(double e, double r, double acc){
		double r0=1e-3;
		if(r<r0) return r*(1-r);
		else{
			vector f0 = new vector(r0*(1-r0),1-2*r0);
			Func<double,vector,vector> diff_eq=(x,f) => new vector(f[1],-2*(1D/x+e)*f[0]);
			vector frmax = ode.rk45(diff_eq,r0,f0,r,acc:acc,eps:0,h:1e-3);
			double assymp = Exp(-Sqrt(2*Abs(e))*r);
			return frmax[0] - assymp;
		}//else
}//swave
}//main
