using static System.Console;
using static vector3d;
class main{
static void Main(){
	vector3d e_x = new vector3d(1,0,0);
	Write("Defining a vector:\n");
	Write($"e_x = ({e_x.x},{e_x.y},{e_x.z})\n");
	vector3d sum = e_x + e_x;
	Write("Vector addition:\n");
	Write($"e_x+e_x = ({sum.x},{sum.y},{sum.z})\n");
	vector3d diff = e_x - e_x;
	Write("Vector subtraction:\n");
	Write($"e_x-e_x = ({diff.x},{diff.y},{diff.z})\n");
	vector3d mult = 2*e_x;
	Write("Scalar multiplication of vector:\n");
	Write($"2*e_x = ({mult.x},{mult.y},{mult.z})\n");
	vector3d e_y = new vector3d(0,1,0);
	Write("Dot products of unit vectors:\n");
	Write($"e_x*e_x = {vector3d.dot_product(e_x,e_x)}\n");
	Write($"e_x*e_y = {vector3d.dot_product(e_x,e_y)}\n");
	Write("Cross products of unit vectors:\n");
	vector3d cross_x = vector3d.cross_product(e_x,e_x);
	vector3d cross_y = vector3d.cross_product(e_x,e_y);
	Write($"e_x*e_x = ({cross_x.x},{cross_x.y},{cross_x.z})\n");
	Write($"e_x*e_y = ({cross_y.x},{cross_y.y},{cross_y.z})\n");
	Write($"|e_x| = {vector3d.magnitude(e_x)}\n");
} // Main
} // main
