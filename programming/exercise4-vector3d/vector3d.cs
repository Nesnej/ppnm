public struct vector3d{
	public double x,y,z;

	// constructor
	public vector3d(double a, double b, double c){x=a;y=b;z=c;}

	// operators
	static public vector3d operator*(vector3d v, double c){return new vector3d(c*v.x,c*v.y,c*v.z);}
	static public vector3d operator*(double c, vector3d v){return v*c;}
	static public vector3d operator+(vector3d u, vector3d v){return new vector3d(u.x+v.x,u.y+v.y,u.z+v.z);}
	static public vector3d operator-(vector3d u, vector3d v){return new vector3d(u.x-v.x,u.y-v.y,u.z-v.z);}

	// methods
	public static double dot_product(vector3d u, vector3d v){return u.x*v.x + u.y*v.y + u.z*v.z;}
	public static vector3d cross_product(vector3d u, vector3d v){
		return new vector3d(u.y*v.z-u.z*v.y,u.z*v.x-u.x*v.z,u.x*v.y-u.y*v.x);
	}
	public static double magnitude(vector3d v){return dot_product(v,v);}
}
