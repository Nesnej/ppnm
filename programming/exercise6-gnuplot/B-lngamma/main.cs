using static System.Console;
using static System.Math;
class main{
static void Main(){
double eps=1.0/32, dx=1.0/16;
for(double x=0+eps;x<=5;x+=dx) {
	WriteLine("{0,10:f3} {1,15:f8}",x,Log(math.gamma(x)));
	Error.WriteLine("{0,10:f3} {1,15:f8}",x,math.lngamma(x));
	}
Write("\n\n");
for(double x=.5;x<5;x+=.5) Write($"{x}\n");
}//Main
}//main
