using static System.Console;
using static System.Math;
class main{
static void Main(){
double dx=1.0/32;
for(double x=-4.5;x<=4.5;x+=dx){
	WriteLine();
	for(double y=-4.5;y<=4.5;y+=dx){
		complex z = new complex(x,y);
		complex gamma = math.gamma(z);
		WriteLine($"{z.Re} {z.Im} {cmath.abs(gamma)}");
	}//for
}//for
}//Main
}//main
