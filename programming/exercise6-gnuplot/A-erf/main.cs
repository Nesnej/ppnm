using static System.Console;
using static System.Math;
class main{
static void Main(){
double eps=1.0/32, dx=1.0/16;
for(double x=-3+eps;x<=3;x+=dx)
	WriteLine("{0,10:f3} {1,15:f8}",x,math.erf(x));
WriteLine("\n");
for(double x=-2.5;x<=2.5;x+=0.5)
	WriteLine($"{x}");
}//Main
}//main
