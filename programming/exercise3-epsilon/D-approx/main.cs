using static System.Console;
using static Approx;
class main{
	static void Main(){
		Write("The following numbers are identical to either an absolute difference\n");
		Write($"fo 1e-9 or a relative difference of 0.5e-9:\n");
		Write($"(1,2) -> {approx(1D,2D)}\n");
		Write($"(1e-9,2e-9) -> {approx(1e-9,2e-9)}\n");
		Write($"(1e-10,2e-10) -> {approx(1e-10,2e-10)}\n");
		Write($"(1e-11,2e-11) -> {approx(1e-11,2e-11)}\n");
		Write($"(1e-12,2e-12) -> {approx(1e-12,2e-12)}\n");
		Write($"(1e-15,2e-15) -> {approx(1e-15,2e-15)}\n");
		Write($"(1e-20,2e-20) -> {approx(1e-20,2e-20)}\n");
	}
}
