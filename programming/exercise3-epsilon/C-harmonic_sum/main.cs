using static System.Console;
//using static System.Math;
using static cmath;
class main{
	static void Main(){
		int max = int.MaxValue;

		float float_sum_up = 1F;
		double double_sum_up = 1D;
		for(int i=2; i<max; i++){
			float_sum_up += 1F/i;
			double_sum_up += 1D/i;
		}
		Write($"float_sum_up = {float_sum_up}\n");
		Write($"double_sum_up = {double_sum_up}\n");

		float float_sum_down = 1F/max;
		double double_sum_down = 1D/max;
		for(int i=max-1; i>0; i--){
			float_sum_down += 1F/i;
			double_sum_down += 1D/i;
		}
		Write($"float_sum_down = {float_sum_down}\n");
		Write($"double_sum_down = {double_sum_down}\n\n");
		Write($"When performing the sum upwards one starts with the largest numbers\n");
		Write($"and then add smaller and smaller numbers, whereas performing the sum\n");
		Write($"downwards one starts with the smallest numbers. When two number of different\n");
		Write($"scales are added the round off error is large compared to the addition of numbers\n");
		Write($"of similar scale. Thus performing the sum upwards should yield a lower number\n");
		Write($"than when doing it downwards, as comparitatively large parts of the small numbers\n");
		Write($"are lost in the rounding. This problem should be worse for floating points than doubles\n");
		Write($"since doubles have more memory, and thus more digits, available than floating points.\n\n");

		Write($"The harmonic sum is a slowly diverging series, but since the divergence is so slow\n");
		Write($"it will likely be numerically convergent due to round off errors. This will be seen\n");
		Write($"in this case as the sums terminating at quite modest numbers far from infinity even\n");
		Write($"though the sum runs to the maximum integer.\n");
	}
}
