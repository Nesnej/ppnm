using static System.Math;
using static System.Console;
using static cmath;
class main{
	static void Main(){
	double x=1D;
	while(x+1!=1){x/=2;}
	Write($"My machine epsilon for doubles                   : {2*x}\n");
	Write($"The estimated machine epsilon for doubles        : {Pow(2,-52)}\n");
	float y=1F;
	while((float)(1F+y) != 1F){y/=2F;}
	Write($"my machine epsilon for floating points           : {2F*y}\n");
	Write($"The estimated machine epsilon for floating points: {Pow(2,-23)}\n");
	}
}
