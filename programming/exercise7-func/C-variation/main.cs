using System;
using static System.Math;
using static System.Console;
using static math;
class main{
static void Main(){
	double dalpha=1.0/16;
	for(double alpha=0+dalpha;alpha<=3;alpha+=dalpha){
		double E = H_expect(alpha)/overlap(alpha);
	WriteLine($"{alpha} {E}");
	} //for
} //Main

static double overlap(double alpha){
	const double inf = System.Double.PositiveInfinity;
	Func<double,double> f = (x) => Exp(-alpha*x*x);
	return quad.o8av(f,-inf,inf,acc:1e-9,eps:1e-9);
} //overlap

static double H_expect(double alpha){
	const double inf = System.Double.PositiveInfinity;
	Func<double,double> f = (x) => (-alpha*alpha*x*x/2 + alpha/2 + x*x/2)*Exp(-alpha*x*x);
	return quad.o8av(f,-inf,inf);
} //H_expect

} //main
