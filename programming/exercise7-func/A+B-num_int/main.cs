using System;
using static System.Math;
using static System.Console;
using static math;
class main{
static void Main(){
	const double inf = System.Double.PositiveInfinity;
	Func<double,double> A1 = (x) => Log(x)/Sqrt(x);
	double a=0, b=1;
	Write($"integral ln(x)/sqrt(x) [{a},{b}] = {quad.o8av(A1,a, b)}\ntabulated value = -4\n");

	a = -inf; b = inf;	
	Func<double,double> A2 = (x) => Exp(-x*x);
	Write($"integral exp(-x*x) [{a},{b}] = {quad.o8av(A2,a, b)}\ntabulated value = {Sqrt(PI)}\n");

	a = 0; b = 1;
	for(int i=1;i<10;i++){
		Func<double,double> A3 = (x) => Pow(Log(1/x),i);
		Write($"integral ln(1/x)^{i} [{a},{b}] = {quad.o8av(A3,a, b)}\ntabulated value = {math.gamma(i+1)}\n");
	}

	a = 0; b = inf;	
	Func<double,double> B2 = (x) => x/(Exp(x)-1);
	Write($"integral x/(exp(x)-1) [{a},{b}] = {quad.o8av(B2,a, b)}\ntabulated value = {PI*PI/6}\n");

	Func<double,double> B3 = (x) => Sqrt(x)*Exp(-x);
	Write($"integral sqrt(x)*exp(-x) = {quad.o8av(B3,a, b)}\ntabulated value = {Sqrt(PI)/2}\n");

	double z = 2.5;
	Func<double,double> B1 = (x) => Pow(x,z-1)*Exp(-x);
	Write($"integral x^{z-1}*exp(-x) [{a},{b}] = {quad.o8av(B1,a, b)}\n");
	Write($"tabulated value = {math.gamma(z)}\n");
} //Main
} //main
