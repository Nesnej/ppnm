using static System.Console;
using static System.Math;
class main{
static void Main(){
double  dx=1.0/16;//,eps=1.0/32;
for(double x=0;x<=2*PI;x+=dx)
	WriteLine("{0,10:f3} {1,15:f8}",Sin(x),Cos(x));
}//Main
}//main
