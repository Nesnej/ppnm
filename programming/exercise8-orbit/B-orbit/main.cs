using static System.Console;
using static System.Math;
using System;
using System.Collections;
using System.Collections.Generic;
class main{
static void Main(string[] args){
	double u0=1,u1=0,a=0,b=2,eps=0;
	foreach(string s in args){
		string[] ws=s.Split('=');
		if(ws[0]=="a") a=double.Parse(ws[1]);
		if(ws[0]=="u0") u0=double.Parse(ws[1]);
		if(ws[0]=="u1") u1=double.Parse(ws[1]);
		if(ws[0]=="b") b=double.Parse(ws[1]);
		if(ws[0]=="eps") eps=double.Parse(ws[1]);
	}
	b *= PI;
	Func<double,vector,vector> orb=(phi,u) => new vector(u[1],1+u[0]*(u[0]*eps-1));
	vector ua = new vector(u0,u1);
	List<double> phis = new List<double>();
	List<vector> us = new List<vector>();
	ode.rk23(orb,a,ua,b,xlist:phis,ylist:us,acc:1e-4,eps:1e-4);
	for(int i=0;i<phis.Count;i++)
		WriteLine($"{phis[i]} {us[i][0]}");
} //Main
} //main
