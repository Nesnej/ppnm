using static System.Console;
using System;
using System.Collections;
using System.Collections.Generic;
static class main{
static void Main(){
	double y0 = .5;
	double a = 0;
	double b = 3;
	Func<double,vector,vector> log=(x,y) => new vector(y[0]*(1-y[0]));
	vector ya = new vector(y0);
	List<double> xs = new List<double>();
	List<vector> ys = new List<vector>();
	ode.rk23(log,a,ya,b,xlist:xs,ylist:ys);
	for(int i=0;i<xs.Count;i++)
		WriteLine($"{xs[i]} {ys[i][0]}");
} //Main
} //main
