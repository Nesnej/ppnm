using static System.Console;
using System;
using static System.Math;
static class main{
static void Main(){
	double eps=1.0/32, dx=1.0/16;
	Func<double,double> log=(x) => 1/(1+Exp(-x));
	for(double x=0+eps;x<=3;x+=dx)
		WriteLine("{0,10:f3} {1,15:f8}",x,log(x));
} //Main
} //main
