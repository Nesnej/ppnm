using System.IO;
using static System.Math;
public class main{
public static int Main(string[] args){
	if(args.Length<2) return 1;
	string infilename = args[0];
	string outfilename = args[1];
	var infile = new StreamReader(infilename);
	var outfile = new StreamWriter(outfilename);
	outfile.Write("x	cos(x)	sin(x)\n");
	while(true){
		string line = infile.ReadLine();
		if(line==null) break;
		string[] ws = line.Split(' ',',','\t');
		foreach(string w in ws){
			double x = double.Parse(w);
			outfile.Write($"{x}	{Cos(x)}	{Sin(x)}\n");
		}//foreach
	}//while
	infile.Close();
	outfile.Close();
	return 0;
}//Main
}//main
