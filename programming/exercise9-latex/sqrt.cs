using System;
public static class nonef{
public static double sqrt(double x){
	if(x<0){
		Console.Error.Write("The square root of a negative number is illdefined in the real numbers. Returned NaN.\n");
		return Double.NaN;
	}
	if(x==0){return 0;}
	if(x>2){return sqrt(2D)*sqrt(x/2);}
	if(x<1){return sqrt(2*x)/sqrt(2D);}
	Func<double,vector,vector> sqrteq=(t,y)=>new vector(1/(2*y[0]));
	double a=1;
	vector ya=new vector(1D);
	vector res=ode.rk23(sqrteq,a,ya,x,acc:1e-3,eps:1e-3);
	return res[0];
} //sqrt
} //nonef
