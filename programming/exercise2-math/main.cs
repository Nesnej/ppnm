using static System.Console;
using static System.Math;
using static cmath;
class main{
	static void Main(){
		Write($"sqrt(2) = {sqrt(2)}\n");
		Write($"exp(i) = {exp(complex.I)}\n");
		Write($"exp(i*pi) = {exp(complex.I*PI)}\n");
		Write($"i^i = {complex.I.pow(complex.I)}\n");
		Write($"sin(i*pi) = {sin(complex.I*PI)}\n");
		Write($"sinh(i) = {sinh(complex.I)}\n");
		Write($"cosh(i) = {cosh(complex.I)}\n");
		Write($"sqrt(-1) = {sqrt(new complex(-1,0))}\n");
		Write($"sqrt(i) = {sqrt(complex.I)}\n");
	}
}
